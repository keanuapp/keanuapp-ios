#!/bin/sh

# https://medium.com/swiftly-tech/how-to-test-push-notifications-in-xcode-11-4-simulator-44ab5ba0efd5
# https://developer.apple.com/documentation/usernotifications/setting_up_a_remote_notification_server/generating_a_remote_notification
# https://developer.apple.com/documentation/usernotifications/setting_up_a_remote_notification_server/pushing_background_updates_to_your_app

# Get absolute path to this script.
DIR=$(cd `dirname $0` && pwd)

DEVICEID=${1:-'booted'}
FILE=${2:-'silent-push'}

xcrun simctl push "${DEVICEID}" com.netzarchitekten.keanu "${DIR}/${FILE}.apns"
