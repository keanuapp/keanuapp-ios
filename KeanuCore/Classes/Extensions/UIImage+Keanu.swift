//
//  UIImage+Keanu.swift
//  Keanu
//
//  Created by N-Pex on 14.11.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit
import AVFoundation

extension UIImage {

    // MARK: Scaling

    /**
     Keanu's default size for images to share: 1080 x 1080 pixel.
    */
    public static let defaultSize = CGSize(width: 1080, height: 1080)

    /**
     Keanu's default JPEG compression quality setting, when exporting to JPEG: 90%.
    */
    public static let defaultJpegCompressionQuality: CGFloat = 0.9

    /**
     If the image width or the height is larger than the given size, resize the
     image to fill the entire area.

     - parameter size: The target size.
     - returns: A scaled image or self, if self is not larger than the given size.
     */
    public func aspectFill(size: CGSize) -> UIImage? {
        if self.size.width > size.width || self.size.height > size.height {

            let scaleFactor = max(self.size.width / size.width,
                                  self.size.height / size.height)

            return render(with: CGSize(width: self.size.width / scaleFactor,
                                       height: self.size.height / scaleFactor))
        }

        return self
    }

    /**
     Scales the image to the given size (*up* or *down*), keeping the aspect ratio.

     - parameter size: The target size.
     - returns: A scaled image or a copy of self, if the scaling failed.
    */
    public func scale(to size: CGSize = defaultSize) -> UIImage {
        let scaled = AVMakeRect(aspectRatio: self.size, insideRect: CGRect(origin: .zero, size: size))

        return render(with: scaled.size) ?? self.copy() as! UIImage
    }

    /**
     Scales the image *DOWN* to the given size, keeping the aspect ratio,
     *IF* it is bigger than the given size.

     - parameter size: The target size.
     - returns: A scaled image or a copy of self, if the scaling failed, or if
       self actually is smaller than the given size.
     */
    public func reduce(to size: CGSize = defaultSize) -> UIImage {
        if self.size.width > size.width || self.size.height > size.height {
            return scale(to: size)
        }

        return self.copy() as! UIImage
    }

    /**
     Render the image with the given size.

     - parameter size: The target size to render to.
     - returns: A new image rendered to the given size.
    */
    private func render(with size: CGSize) -> UIImage? {
        UIGraphicsBeginImageContext(size)
        UIGraphicsGetCurrentContext()?.interpolationQuality = .high

        draw(in: CGRect(origin: .zero, size: size))

        let rendering = UIGraphicsGetImageFromCurrentImageContext()

        UIGraphicsEndImageContext()

        return rendering
    }


    // MARK: Export

    /**
     Returns the data for the specified image in JPEG format with a default
     compression quality of `UIImage.defaultJpegCompressionQuality`.
     (Which should be 0.9.)

     If the image object’s underlying image data has been purged, calling this
     function forces that data to be reloaded into memory.


     - returns: A data object containing the JPEG data, or nil if there was a
       problem generating the data. This function may return nil if the image has
       no data or if the underlying `CGImageRef` contains data in an unsupported
       bitmap format.
    */
    public func jpegData() -> Data? {
        return self.jpegData(compressionQuality: UIImage.defaultJpegCompressionQuality)
    }


    // MARK: Video


    /**
     Get a thumbnail for the video at the given URL.

     Adapted from here: https://stackoverflow.com/questions/33953841/how-to-get-thumbnail-image-of-video-picked-from-uipickerviewcontroller-in-swift

     - parameter url: The URL to a video file.
     - returns: A still image from the video at second 2 or earlier, if shorter or `nil` if an error occured.
     */
    public class func thumbnailForVideo(at url: URL) -> UIImage? {
        let asset = AVAsset(url: url)

        let generator = AVAssetImageGenerator(asset: asset)
        generator.appliesPreferredTrackTransform = true

        let d = asset.duration
        let time = CMTimeMinimum(d, CMTime(seconds: 2, preferredTimescale: d.timescale))

        let imageRef = try? generator.copyCGImage(at: time, actualTime: nil)

        return imageRef != nil ? UIImage(cgImage: imageRef!) : nil
    }
}
