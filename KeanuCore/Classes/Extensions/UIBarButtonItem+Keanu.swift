//
//  UIBarButtonItem+Keanu.swift
//  KeanuCore
//
//  Created by Benjamin Erhart on 28.05.20.
//

import UIKit

extension UIBarButtonItem {

    public convenience init(buttonType: UIButton.ButtonType, target: Any, action: Selector) {
        let button = UIButton(type: buttonType)
        button.addTarget(target, action: action, for: .touchUpInside)

        self.init(customView: button)
    }

    public convenience init(materialIcon: String, target: Any, action: Selector) {
        self.init(buttonType: .system, target: target, action: action)

        if let button = customView as? UIButton {
            button.setTitle(materialIcon)
            button.titleLabel?.font = .materialIcons()
        }
    }

    public var materialIcon: String? {
        get {
            return (customView as? UIButton)?.title(for: .normal)
        }
        set {
            (customView as? UIButton)?.setTitle(newValue)
        }
    }
}
