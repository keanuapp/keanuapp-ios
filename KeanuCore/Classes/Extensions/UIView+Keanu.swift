//
//  UIView+Keanu.swift
//  KeanuCore
//
//  Created by Benjamin Erhart on 13.04.22.
//

import Foundation

extension UIView {

    public func superview<T>(of type: T.Type) -> T? {
        superview as? T ?? superview?.superview(of: type)
    }

    public func copyView<T: UIView>() -> T {
        let archiver = NSKeyedArchiver(requiringSecureCoding: false)
        archiver.encode(self, forKey: "temp")

        let unarchiver = try! NSKeyedUnarchiver(forReadingFrom: archiver.encodedData)
        unarchiver.requiresSecureCoding = false

        return unarchiver.decodeObject(of: T.self, forKey: "temp")!
    }
}
