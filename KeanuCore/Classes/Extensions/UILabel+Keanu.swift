//
//  UILabel+Keanu.swift
//  KeanuCore
//
//  Created by Benjamin Erhart on 25.02.20.
//

import UIKit

extension UILabel {

    /**
     Sets `attributedText` with an leading icon from the `MaterialIcons-Regular` font and
     some descriptive text written in system font.

     Honors text direction.

     - parameter icon: The leading icon from the `MaterialIcons-Regular` font.
     - parameter text: The trailing label text.
     - parameter iconSize: Font size of the icon, defaults to 24.
     - parameter textFontSize: Font size of the trailing label text.
     - returns: `self` for fluency.
     */
    @discardableResult
    public func setIconAndText(_ icon: String, _ text: String, iconSize: CGFloat = 24,
                               textSize: CGFloat = 17) -> UILabel {

        let label: NSMutableAttributedString

        let iconAttr: [NSAttributedString.Key: Any] = [
            .font: UIFont.materialIcons(ofSize: iconSize)]

        let textAttr: [NSAttributedString.Key: Any] = [
            .font: UIFont.systemFont(ofSize: textSize),
            .baselineOffset: (iconSize - textSize) / 2] // vertically center

        if UIView.userInterfaceLayoutDirection(for: semanticContentAttribute) == .rightToLeft {
            label = NSMutableAttributedString(string: text, attributes: textAttr)
            label.append(NSAttributedString(string: " \(icon)", attributes: iconAttr))
        }
        else {
            label = NSMutableAttributedString(string: "\(icon) ", attributes: iconAttr)
            label.append(NSAttributedString(string: text, attributes: textAttr))
        }

        attributedText = label

        return self
    }
}
