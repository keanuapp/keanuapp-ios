//
//  MXKAttachment+Keanu.swift
//  KeanuCore
//
//  Created by N-Pex on 23.08.19.
//

import Foundation

extension MXKAttachment {

    // TODO: when we remove MXKAttachment, there is a MXEvent.isHtmlAttachment property instead.
    public var isHtml: Bool {
        let htmlExtension = originalFileName?.lowercased().hasSuffix(".html") ?? false
        let htmlMime = (contentInfo?["mimetype"] as? String)?.starts(with: "text/html") ?? false
        return htmlExtension || htmlMime
    }
    
    /**
     Make a more appealing display name from a filename, i.e. remove extension and URL encoding.
     */
    public var fileDisplayName: String? {
        if let path = originalFileName as NSString? {
            return path.deletingPathExtension.removingPercentEncoding?.replacingOccurrences(of: "+", with: " ")
        }
        return originalFileName
    }
    
}
