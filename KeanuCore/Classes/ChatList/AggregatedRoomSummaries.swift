//
//  AggregatedRoomSummaries.swift
//  KeanuCore
//
//  Created by N-Pex on 07.01.20.
//  Copyright © 2020 Guardian Project. All rights reserved.

import MatrixSDK

/**
 Base class for creating data sources of MXRoomSummary objects.
 
 Basically this handles an array of MXRoomSummary objects. The class handles all
 underlying logic of listening to MX session changes, room adds/removes etc.
 */
open class AggregatedRoomSummaries: NSObject {
    
    open var roomSummaries: [MXRoomSummary] = []
    open var summarySessionMap: [MXRoomSummary:MXSession] = [:]
    
    // Notification center listener objects
    open var observers: [NSObjectProtocol] = []
    
    public override init() {
        super.init()

        let nc = NotificationCenter.default
        
        observers.append(nc.addObserver(forName: .mxRoomSummaryDidChange, object: nil, queue: .main) { (notification) in
            if let singleSummary = notification.object as? MXRoomSummary {
                self.didChangeRoomSummary(singleSummary)
            } else {
                self.didChangeAllRoomSummaries()
            }
        })
        
        observers.append(nc.addObserver(forName: .mxSessionNewRoom, object: nil, queue: .main) { (notification) in
            if let session = notification.object as? MXSession {
                self.syncSessionRoomSummaries(session)
            }
        })
        
        observers.append(nc.addObserver(forName: .mxSessionDidLeaveRoom, object: nil, queue: .main) { (notification) in
            if let session = notification.object as? MXSession {
                self.syncSessionRoomSummaries(session)
            }
        })
        
        observers.append(nc.addObserver(forName: .mxSessionDirectRoomsDidChange, object: nil, queue: .main) { (notification) in
            if let session = notification.object as? MXSession {
                self.syncSessionRoomSummaries(session)
            }
        })
        
        observers.append(nc.addObserver(forName: .mxSessionStateDidChange, object: nil, queue: .main) { (notification) in
            if let session = notification.object as? MXSession {
                self.syncSessionRoomSummaries(session)
            }
        })
        
        // Add all current sessions
        reload()
    }
    
    deinit {
        observers.forEach({ NotificationCenter.default.removeObserver($0) })
        observers.removeAll()
        self.roomSummaries.removeAll()
        self.summarySessionMap.removeAll()
    }
    
    public func reload() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else {return}
            self.roomSummaries.removeAll()
            self.summarySessionMap.removeAll()
            self.didChangeAllRoomSummaries()
        }

        for account in MXKAccountManager.shared().activeAccounts {
            if let session = account.mxSession {
                syncSessionRoomSummaries(session)
            }
        }
    }
    
    /**
     Sync all room summaries for this session
     */
    open func syncSessionRoomSummaries(_ session: MXSession) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else {return}
            self.summarySessionMap.filter { (arg0) -> Bool in
                let (room, s) = arg0
                return s == session && session.roomSummary(withRoomId: room.roomId) == nil
            }.forEach { (arg0) in
                let (room, _) = arg0
                self.summarySessionMap.removeValue(forKey: room)
                self.roomSummaries.removeAll { (summary) -> Bool in
                    summary.roomId == room.roomId
                }
                self.didRemoveRoomSummary(room)
            }

            for room in session.rooms {
                guard let summary = session.roomSummary(withRoomId: room.roomId) else {
                    continue
                }

                if !self.roomSummaries.contains(summary) && self.checkValidRoomSummary(summary) {
                    self.summarySessionMap[summary] = session
                    self.roomSummaries.append(summary)
                    self.didAddRoomSummary(summary)
                }
            }
        }
    }

    func checkValidRoomSummary(_ roomSummary: MXRoomSummary) -> Bool {
        // Never show room that should be invisible
        guard !roomSummary.hiddenFromUser else { return false }
        guard !roomSummary.isConferenceUserRoom else { return false }
        return shouldAddRoomSummary(roomSummary)
    }
    
    open func shouldAddRoomSummary(_ roomSummary: MXRoomSummary) -> Bool {
        return true
    }
    
    open func didAddRoomSummary(_ roomSummary: MXRoomSummary) {
        
    }

    open func didRemoveRoomSummary(_ roomSummary: MXRoomSummary) {
        
    }
    
    open func didChangeRoomSummary(_ roomSummary: MXRoomSummary) {
        
    }
    
    open func didChangeAllRoomSummaries() {
        
    }
}



