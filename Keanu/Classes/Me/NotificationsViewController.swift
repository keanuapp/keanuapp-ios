//
//  NotificationsViewController.swift
//  Keanu
//
//  Created by N-Pex on 15.6.22.
//

import UIKit
import MatrixSDK
import KeanuCore
import Eureka
import ViewRow

open class NotificationsViewController: FormViewController {
    
    // MARK: Properties
    var session: MXSession? {
        didSet {
            if self.isViewLoaded {
                self.createForm()
            }
        }
    }
    
    var nc: MXNotificationCenter? {
        get {
            return self.session?.notificationCenter
        }
    }
    
    let bundle: Bundle = Bundle(for: NotificationsViewController.self)
    var notificationListeners: [NSObjectProtocol] = []
    var notificationEnabled: Bool {
        get {
            if let rule = nc?.rule(byId: ".m.rule.master") {
                return !rule.enabled
            }
            return true
        }
    }
    
    open var keanuPushRule : MXPushRule? {
        get {
            if let rule = nc?.rule(byId: NotificationSettings.keanuPushRuleId) {
                return rule
            }
            return nil
        }
    }

    open var keanuPushRuleEncrypted : MXPushRule? {
        get {
            if let rule = nc?.rule(byId: NotificationSettings.keanuPushRuleIdEncrypted) {
                return rule
            }
            return nil
        }
    }

    class func createKeanuPushRules(session: MXSession) {
        if let nc = session.notificationCenter {
            if nc.rule(byId: NotificationSettings.keanuPushRuleId) == nil {
                nc.addOverrideRule(withId: NotificationSettings.keanuPushRuleId, conditions: [["kind": "event_match","key": "type","pattern": "m.room.message"]], notify: true, sound: false, highlight: false)
            }
            if nc.rule(byId: NotificationSettings.keanuPushRuleIdEncrypted) == nil {
                nc.addOverrideRule(withId: NotificationSettings.keanuPushRuleIdEncrypted, conditions: [["kind": "event_match","key": "type","pattern": "m.room.encrypted"]], notify: true, sound: false, highlight: false)
            }
        }
    }
    
    var ringTones: [String: URL] = [:]
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        loadRingtones()
        createForm()
    }
    
    open func loadRingtones() {
        // Find all ringtones.
        let directoryURL = Bundle.main.bundleURL
        let localFileManager = FileManager()
        
        let resourceKeys = Set<URLResourceKey>([.nameKey, .isDirectoryKey])
        let directoryEnumerator = localFileManager.enumerator(at: directoryURL, includingPropertiesForKeys: Array(resourceKeys), options: .skipsHiddenFiles)!
        
        for case let fileURL as URL in directoryEnumerator {
            guard let resourceValues = try? fileURL.resourceValues(forKeys: resourceKeys),
                  let isDirectory = resourceValues.isDirectory,
                  let name = resourceValues.name
            else {
                continue
            }
            
            if !isDirectory, name.hasSuffix(".m4r") {
                ringTones[name] = fileURL
            }
        }
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.notificationListeners.append(NotificationCenter.default.addObserver(forName: NSNotification.Name(kMXNotificationCenterDidUpdateRules), object: nil, queue: OperationQueue.main) { notification in
            self.view.isUserInteractionEnabled = true
            self.form.allSections.forEach { section in
                section.forEach({ $0.updateCell() })
            }
        })
        self.notificationListeners.append(NotificationCenter.default.addObserver(forName: NSNotification.Name(kMXNotificationCenterDidFailRulesUpdate), object: nil, queue: OperationQueue.main) { notification in
            self.view.isUserInteractionEnabled = true
            self.form.allSections.forEach { section in
                section.forEach({ $0.updateCell() })
            }
        })

    }
    
    open override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.notificationListeners.forEach({ listener in
            NotificationCenter.default.removeObserver(listener)
        })
        self.notificationListeners = []
    }
    
    public init() {
        super.init(style: .grouped)
    }
    
    public override init(style: UITableView.Style) {
        super.init(style: style)
    }
    
    required public init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    // MARK: Public Methods
    
    open func createForm() {
        form.removeAll()
        form +++ SwitchRow()
        { row in
            row.title = "Notifications".localize()
            row.tag = "Notifications"
            row.value = self.notificationEnabled
        }
        .onChange { row in
            if let rule = self.nc?.rule(byId: ".m.rule.master") {
                self.view.isUserInteractionEnabled = false
                self.nc?.enableRule(rule, isEnabled: !rule.enabled)
            }
        }
        
        form +++ SelectableSection<ListCheckRow<String>>(nil, selectionType: .singleSelection(enableDeselection: false)) { section in
            section.onSelectSelectableRow = { (cell, cellRow) in
                guard let nc = self.nc else { return }
                self.view.isUserInteractionEnabled = false
                switch cellRow.value {
                case "always":
                    if let rule = self.keanuPushRule {
                        nc.enableRule(rule, isEnabled: true)
                    }
                    if let rule = self.keanuPushRuleEncrypted {
                        nc.enableRule(rule, isEnabled: true)
                    }
                    NotificationSettings.shared.notifyCondition = NotificationSettings.NotificationCondition.Always
                    break
                    
                case "mentions":
                    if let rule = self.keanuPushRule {
                        nc.enableRule(rule, isEnabled: true)
                    }
                    if let rule = self.keanuPushRuleEncrypted {
                        nc.enableRule(rule, isEnabled: true)
                    }
                    NotificationSettings.shared.notifyCondition = NotificationSettings.NotificationCondition.Mentions
                    break
                    
                default:
                    if let rule = self.keanuPushRule {
                        nc.enableRule(rule, isEnabled: false)
                    }
                    if let rule = self.keanuPushRuleEncrypted {
                        nc.enableRule(rule, isEnabled: false)
                    }
                    NotificationSettings.shared.notifyCondition = NotificationSettings.NotificationCondition.Individual
                    break
                }
            }
        }
        <<< LabelRow("whenToNotify") {
            $0.title = "When to notify".localize()
        }
        .cellSetup({ cell, row in
            cell.imageView?.image = UIImage(named: "ic_notify", in: self.bundle, compatibleWith: nil)
            cell.imageView?.tintColor = UIView.appearance().tintColor
        })
        for option in [["name":"Anytime there’s a new message".localize(),"value":NotificationSettings.NotificationCondition.Always.rawValue]
                       ,["name":"Only if I’m mentioned".localize(),"value":NotificationSettings.NotificationCondition.Mentions.rawValue]
                       ,["name":"Choose per group".localize(),"value": NotificationSettings.NotificationCondition.Individual.rawValue]
        ] {
            form.last! <<< ListCheckRow<String>(option["name"]){ listRow in
                listRow.title = option["name"]
                listRow.selectableValue = option["value"]
                listRow.value = option["value"]
            }
            .cellUpdate({ cell, row in
                var on = false
                switch row.selectableValue {
                case NotificationSettings.NotificationCondition.Always.rawValue:
                    on = NotificationSettings.shared.notifyCondition == NotificationSettings.NotificationCondition.Always
                    break
                case NotificationSettings.NotificationCondition.Mentions.rawValue:
                    on = NotificationSettings.shared.notifyCondition == NotificationSettings.NotificationCondition.Mentions
                    break
                default:
                    on = NotificationSettings.shared.notifyCondition == NotificationSettings.NotificationCondition.Individual
                    break
                }
                if on {
                    cell.accessoryView = UIImageView(image: UIImage(named: "ic_radio_on", in: self.bundle, compatibleWith: nil))
                } else {
                    cell.accessoryView = UIImageView(image: UIImage(named: "ic_radio_off", in: self.bundle, compatibleWith: nil))
                }
            })
        }
        
        form +++ LabelRow("notifyNewOptions") {
            $0.title = "New message notifications".localize()
        }
        .cellSetup({ cell, row in
            cell.imageView?.image = UIImage(named: "ic_notif_all", in: self.bundle, compatibleWith: nil)
            cell.imageView?.tintColor = UIView.appearance().tintColor
            if let section = row.section {
                section.footer = HeaderFooterView(stringLiteral: "If you’re getting new message notifications, they will sound like this.".localize())
            }
        })
        <<< SwitchRow("all_sound")
        { row in
            row.title = "Sound".localize()
            row.value = NotificationSettings.shared.soundAll
        }
        .onChange { row in
            let useSound = row.value ?? true
            NotificationSettings.shared.soundAll = useSound
        }
        <<< SwitchRow()
        { row in
            row.title = "Vibration".localize()
            row.value = NotificationSettings.shared.vibrateAll
        }
        .onChange { row in
            NotificationSettings.shared.vibrateAll = row.value ?? false
        }
        <<< PushRow<String>("all_ringtone") {
            $0.title = "Ringtone".localize()
            $0.value = NotificationSettings.shared.ringtoneAll ?? "default"
            $0.disabled =  "$all_sound == false"
            $0.options = Array(ringTones.keys)
            $0.onChange { row in
                NotificationSettings.shared.ringtoneAll = row.value
            }
        }
        form +++ LabelRow("notifyMentions") {
            $0.title = "Mentions".localize()
        }
        .cellSetup({ cell, row in
            cell.imageView?.image = UIImage(named: "ic_notif_mentions", in: self.bundle, compatibleWith: nil)
            cell.imageView?.tintColor = UIView.appearance().tintColor
            if let section = row.section {
                section.footer = HeaderFooterView(stringLiteral: "If you’re mentioned, the notification will sound like this.".localize())
            }
        })
        
        
        <<< SwitchRow("mentions_sound")
        { row in
            row.title = "Sound".localize()
            row.value = NotificationSettings.shared.soundMentions
        }
        .onChange { row in
            let useSound = row.value ?? true
            NotificationSettings.shared.soundMentions = useSound
        }
        <<< SwitchRow()
        { row in
            row.title = "Vibration".localize()
            row.value = NotificationSettings.shared.vibrateMentions
        }
        .onChange { row in
            NotificationSettings.shared.vibrateMentions = row.value ?? true
        }
        <<< PushRow<String>("mentions_ringtone") {
            $0.title = "Ringtone".localize()
            $0.disabled =  "$mentions_sound == false"
            $0.value = NotificationSettings.shared.ringtoneMentions ?? "default"
            $0.options = Array(ringTones.keys)
            $0.onChange {row in
                NotificationSettings.shared.ringtoneMentions = row.value
            }
        }
    }
    
    func getPushRuleTweak(ruleName: String, tweakName: String) -> String? {
        if let rule = self.nc?.rule(byId: ruleName) {
            if let tweak = (rule.actions as? [MXPushRuleAction])?.first(where: { action in
                return action.actionType == MXPushRuleActionTypeSetTweak && action.parameters["set_tweak"] as? String == tweakName
            }) {
                return tweak.parameters["value"] as? String
            }
        }
        return nil
    }
    
    func setPushRuleTweak(ruleName: String, tweakName: String, value: String?) {
        if let rule = self.nc?.rule(byId: ruleName) {
            if let tweak = (rule.actions as? [MXPushRuleAction])?.first(where: { action in
                return action.actionType == MXPushRuleActionTypeSetTweak && action.parameters["set_tweak"] as? String == tweakName
            }) {
                if let value = value {
                    tweak.parameters["value"] = value
                } else {
                    if let index = rule.actions.firstIndex(where: { $0 as! MXPushRuleAction == tweak }) {
                        rule.actions.remove(at: index)
                    }
                }
            } else if let value = value {
                let action = MXPushRuleAction()
                action.actionType = MXPushRuleActionTypeSetTweak
                action.action = kMXPushRuleActionStringSetTweak
                action.parameters = ["set_tweak": tweakName, "value" : value]
                rule.actions.append(action)
            }
            self.nc?.enableRule(rule, isEnabled: rule.enabled)
        }
    }
}
