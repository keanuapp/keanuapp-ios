//
//  ChatListInviteCell.swift
//  Keanu
//
//  Created by N-Pex on 01.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit
import MatrixSDK
import KeanuCore

open class NewDeviceCell: UITableViewCell {

    open class var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }

    open class var defaultReuseId: String {
        return String(describing: self)
    }

    open class var height: CGFloat {
        return 120
    }

    @IBOutlet weak var titleLb: UILabel! {
        didSet {
            titleLb.text = "New device logged in".localize()
        }
    }

    @IBOutlet weak var messageLb: UILabel!

    @IBOutlet weak var detailsBt: UIButton! {
        didSet {
            detailsBt.setTitle("Details".localize())
        }
    }

    open func apply(_ device: ChatListDataSource.Device) -> NewDeviceCell {
        messageLb.text = "% logged into your account. Please confirm it was you.".localize(value: device.info.displayName ?? device.info.deviceId)

        return self
    }
}
