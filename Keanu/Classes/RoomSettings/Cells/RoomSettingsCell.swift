//
//  RoomSettingsCell.swift
//  Keanu
//
//  Created by Benjamin Erhart on 25.02.20.
//

import UIKit
import KeanuCore

open class RoomSettingsCell: UITableViewCell {

    open class var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }

    public class var defaultReuseId: String {
        return String(describing: self)
    }

    public static let height: CGFloat = 96

    @IBOutlet weak var avatar: AvatarView!

    @IBOutlet weak var nameLb: UILabel!

    @IBOutlet weak var topicLb: UILabel!

    @IBOutlet weak var editNameBt: UIButton!
    @IBOutlet weak var editNameBtWidth: NSLayoutConstraint!
    
    @IBOutlet weak var editTopicBt: UIButton!
    @IBOutlet weak var editTopicBtWidth: NSLayoutConstraint!
}
