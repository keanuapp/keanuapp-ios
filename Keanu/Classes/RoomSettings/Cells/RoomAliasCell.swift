//
//  RoomAliasCell.swift
//  Keanu
//
//  Created by Benjamin Erhart on 07.04.20.
//

import UIKit

class RoomAliasCell: UITableViewCell {

    open class var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }

    public class var defaultReuseId: String {
        return String(describing: self)
    }

    public static let height: CGFloat = 63

    @IBOutlet weak var aliasLb: UILabel!
    
    @IBOutlet weak var editAliasBt: UIButton!
    
    @IBOutlet weak var editAliasBtWidth: NSLayoutConstraint!
}
