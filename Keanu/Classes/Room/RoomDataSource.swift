//
//  RoomDataSource.swift
//  Keanu
//
//  Created by N-Pex on 03.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import KeanuCore
import Foundation

/**
 Data source for a chat.
 */
open class RoomDataSource: NSObject, UITableViewDataSource, UITableViewDelegate, UICollectionViewDataSource {
    
    /**
     Time number of seconds after which we cancel a pending outgoing message and retry.
     */
    public static let kSendTimeout: TimeInterval = 3600
    
    /**
     Static factory callback for creating bubbles that are not handled by the Keanu lib, or to modify ones created by Keanu. Will be called after the base lib has tried to create a bubble.
     
     The callback will have the following parameters:
     - roomBubbleData: The bubble created by the lib, or nil if none.
     - event: The event to create a bubble for.
     - roomState: Room state at the time just before the event.
     */
    public static var bubbleProcessor: ((_ roomDataSource: RoomDataSource, _ event: MXEvent, _ roomBubbleData: RoomBubbleData?, _ roomState: MXRoomState?) -> RoomBubbleData?)?
    
    open weak var delegate: RoomDataSourceDelegate?

    public var pagingDirection: MXTimelineDirection? = nil
    public var pagingSize: UInt = 10
    
    public var sectionHeader = 0
    public var sectionMessages = 1

    /**
     True if the last item in the "roomBubbleData" array should be displayed at top of table (Index path 0,sectionMessages)
     */
    public var reverseOrder = false
    
    open var room: MXRoom
    open var timeline: MXEventTimeline?
    open var timelineListener: MXEventListener?
    open var backPagingDisabled = false
    
    /**
     The bubble data that is handled by the data source.
     */
    open var roomBubbleData = [RoomBubbleData]()
    
    /**
     Map between events and bubble data.
     */
    open var eventRoomBubbleDataMap = [MXEvent: RoomBubbleData]()
    
    /**
     While paging, stores the incoming bubbles(events)
     */
    open var roomBubbleDataPaging = [RoomBubbleData]()
    open var incomingEventFormatter: MXKEventFormatter?
    open var outgoingEventFormatter: MXKEventFormatter?
     
    /**
     Timestamp of last message we consider "read" (or 0 if unknown at the moment)
     */
    open var tsLastRead: UInt64 = 0
    
    /**
     Notification center listener objects

     TODO - listen to more events?
     */
    open var observers = [NSObjectProtocol]()
    private var reactionListener: Any?
    private var replaceListener: Any?

    let queue = DispatchQueue.global(qos: .userInitiated)
    
    public init(room: MXRoom, callInitialize: Bool = true) {
        self.room = room

        super.init()

        if callInitialize {
            initialize()
        }
    }
    
    public func initialize() {
        let room = self.room
        let nc = NotificationCenter.default

        nc.addObserver(self, selector: #selector(didChangeSentState),
                       name: .mxEventDidChangeSentState, object: nil)

        nc.addObserver(self, selector: #selector(didDecrypt),
                       name: .mxEventDidDecrypt, object: nil)

        // Listen to "sessionDidSync" events. We use this to check for unknown
        // devices present in the room, if it is encypted.
        nc.addObserver(self, selector: #selector(sessionDidSync),
                       name: .mxSessionDidSync, object: nil)

        if room.summary.membership == .join {
            room.liveTimeline { [weak self] timeline in
                self?.timeline = timeline
                self?.timelineListener = self?.timeline?.listenToEvents(nil, self!.onEvent) as? MXEventListener

                // Add unsent messages
                for event in room.outgoingMessages() {
                    if event.sentState == MXEventSentStateSent {
                        room.removeOutgoingMessage(event.eventId)
                    }
                    else if event.sentState == MXEventSentStateFailed {
                        // Insert last, we'll do a resend below
                        self?.onEvent(event, .forwards, room.dangerousSyncState)
                    }
                    else if let ts = event.userData?["echoAddedTs"] as? Int64 {
                        self?.onEvent(event, .forwards, room.dangerousSyncState)

                        let then = Date(timeIntervalSince1970: TimeInterval(integerLiteral: ts))
                        let diff = abs(then.timeIntervalSinceNow)

                        // If older than one hour, cancel and retry
                        if diff > RoomDataSource.kSendTimeout {
                            DispatchQueue.main.async {
                                self?.cancelMessage(event: event)
                                self?.resendMessage(event: event)
                            }
                        }
                    }
                }

                DispatchQueue.main.async {
                    self?.resendAllFailedMessages()
                }

                // Do initial backwards pagination
                if let timeline = self?.timeline {
                    timeline.resetPagination()
                    self?.doInitialPagination()
                }

                self?.checkForUnknownDevices()
            }
        }

        createMessageFormatters()
        
        // Listen to replies/reactions!
        reactionListener = room.mxSession.aggregations.listenToReactionCountUpdate(inRoom: room.roomId)
        { [weak self] changes in
            for change in changes {
                DispatchQueue.main.async {
                    if let originalEntry = self?.eventRoomBubbleDataMap.first(where: { $0.key.eventId == change.key }),
                        let indexPath = self?.indexPath(for: originalEntry.value)
                    {
                        self?.delegate?.didChange(
                            deleted: nil, added: nil, changed: [indexPath],
                            live: false, disableScroll: false)
                    }
                }
            }
        }
        
        // Listen to edits.
        replaceListener = room.mxSession.aggregations.listenToEditsUpdate(inRoom: room.roomId)
        { [weak self] replacementEvent in
            guard let replacedEventId = replacementEvent.relatesTo?.eventId else {
                return
            }

            DispatchQueue.main.async {
                if let originalEntry = self?.eventRoomBubbleDataMap.first(where: { $0.key.eventId == replacedEventId })
                {
                    let replacedEvent = originalEntry.key
                    let bubble = originalEntry.value

                    if let indexPath = self?.indexPath(for: bubble),
                       let idx = bubble.events.firstIndex(of: replacedEvent),
                       let updatedEvent = replacedEvent.editedEvent(fromReplacementEvent: replacementEvent)
                    {
                        let completed = { (failedDecryption: [MXEvent]?) in
                            DispatchQueue.main.async {
                                // Replace event in the bubble.
                                bubble.events.remove(at: idx)
                                bubble.events.insert(updatedEvent, at: idx)

                                self?.delegate?.didChange(
                                    deleted: nil, added: nil, changed: [indexPath],
                                    live: false, disableScroll: true)
                            }
                        }

                        // Need to decrypt?
                        self?.queue.async {
                            if updatedEvent.isEncrypted && updatedEvent.clear == nil {
                                room.mxSession?.decryptEvents([updatedEvent], inTimeline: nil, onComplete: completed)
                            }
                            else {
                                completed(nil)
                            }
                        }
                    }
                }
            }
        }
    }

    deinit {
        invalidate()
    }

    open func createMessageFormatters() {
        incomingEventFormatter = MXKEventFormatter(matrixSession: room.mxSession)
        outgoingEventFormatter = MXKEventFormatter(matrixSession: room.mxSession)

        if #available(iOS 11.0, *) {
            if let font = incomingEventFormatter?.defaultTextFont {
                incomingEventFormatter?.defaultTextFont = UIFontMetrics(forTextStyle: .body).scaledFont(for: font)
            }

            if let font = outgoingEventFormatter?.defaultTextFont {
                outgoingEventFormatter?.defaultTextFont = UIFontMetrics(forTextStyle: .body).scaledFont(for: font)
            }
        }
    }

    /**
     Read in the last N events
     */
    open func doInitialPagination() {
        if let lastRead = room.accountData.readMarkerEventId {
            delegate?.pagingStarted(direction: .backwards)

            pageBackwardsToLastRead(lastReadEventId: lastRead)
        }
        else {
            page(direction: .backwards, onlyFromStore: true)
        }
    }
    
    /**
        Page backwards until we find the event we have last read.
     */
    func pageBackwardsToLastRead(lastReadEventId: String) {
        guard timeline?.canPaginate(.backwards) ?? false else {
            return
        }

        pagingDirection = .backwards

        queue.async {
            self.timeline?.paginate(self.pagingSize, direction: .backwards, onlyFromStore: false) { [weak self] response in
                DispatchQueue.main.async {
                    let hasFoundLastRead = self?.roomBubbleDataPaging.contains {
                        return $0.events.contains { event in
                            if event.eventId == lastReadEventId {
                                print("Scroll: found last event: \(event)")
                                return true
                            }

                            return false
                        }
                    } ?? false

                    if response.isSuccess,
                       self?.timeline?.canPaginate(.backwards) ?? false,
                       !hasFoundLastRead,
                       !(self?.backPagingDisabled ?? true)
                    {
                        self?.queue.async {
                            self?.pageBackwardsToLastRead(lastReadEventId: lastReadEventId)
                        }
                    }
                    else {
                        // Done
                        self?.pagingDone(.backwards, success: true, scrollToEventId: lastReadEventId)
                        self?.updateReadReceipts(eventIds: nil)
                    }
                }
            }
        }
    }
    
    /**
     Invalidate the data source. This will stop listeners and observers.
     */
    open func invalidate() {
        delegate = nil

        if let timelineListener = timelineListener {
            timeline?.remove(timelineListener)
        }

        timelineListener = nil
        timeline = nil

        if let reactionListener = reactionListener {
            room.mxSession.aggregations.removeListener(reactionListener)
            self.reactionListener = nil
        }

        if let replaceListener = replaceListener {
            room.mxSession.aggregations.removeListener(replaceListener)
            self.replaceListener = nil
        }

        observers.forEach({ NotificationCenter.default.removeObserver($0) })
        observers.removeAll()

        roomBubbleData.removeAll()

        eventRoomBubbleDataMap.removeAll()
    }
    
    open func onEvent(_ event: MXEvent, _ direction: MXTimelineDirection, _ roomState: MXRoomState?) {
        guard direction == .forwards || isPaging else {
            return
        }

        if event.type == Notification.Name.mxEventTypeStringTyping.rawValue {
            delegate?.onTypingUsersChange(room)
            return
        }
        
        if event.type == kMXEventTypeStringReceipt, let ids = event.readReceiptEventIds() as? [String] {
            updateReadReceipts(eventIds: ids)
            return
        }
        
        if event.isEncrypedAndSentBeforeWeJoined() {
            // Ignore this one and disable backwards paging, we'll get nothing useful.
            backPagingDisabled = true
            return
        }
        
        if event.isRedactedEvent() {
            return
        }
        
        // Is it a replacement (edit)?
        if event.isEdit() {
            acknowledgeEvents(events: [event])
            return
        }
        
        if event.type == kMXEventTypeStringRoomRedaction {
            if let redacts = event.redacts, direction == .forwards
            {
                // This is a room redaction event. It redacts an event sent to the room earlier.
                // If we already have that in our bubble map, we need to find it
                // and mark the original event as redacted, so it is hidden.
                DispatchQueue.main.async { [weak self] in
                    guard let redactedEntry = self?.eventRoomBubbleDataMap.first(where: { $0.key.eventId == redacts })
                    else {
                        return
                    }

                    // We found the original event. Next step is to replace
                    // the original event with a "pruned" one and then tell
                    // the delegate that the cell is updated.
                    let redactedEvent = redactedEntry.key
                    let bubble = redactedEntry.value
                    if !redactedEvent.isRedactedEvent(),
                       let indexPath = self?.indexPath(for: bubble),
                       let idx = bubble.events.firstIndex(of: redactedEvent),
                       let cleanEvent = redactedEvent.prune()
                    {
                        cleanEvent.redactedBecause = event.jsonDictionary()

                        // Not already redacted, do that now.
                        bubble.events.remove(at: idx)
                        bubble.events.insert(cleanEvent, at: idx)

                        self?.delegate?.didChange(
                            deleted: nil, added: nil, changed: [indexPath],
                            live: false, disableScroll: true)
                    }
                }
            }

            return
        }
        
        var data = RoomBubbleData(self, state: roomState, events: [event], type:nil)
        
        // If we have a bubble processor installed, call that.
        if let bubbleProcessor = RoomDataSource.bubbleProcessor {
            data = bubbleProcessor(self, event, data, roomState)
        }

        guard let data = data else {
            return
        }
            
        var deletedIndexPaths = [IndexPath]()
        var deletedBubbles = [RoomBubbleData]()

        // If there's a local echo for this event, remove that.
        if !event.isLocalEvent() {
            if let localEchoEvent = room.pendingLocalEchoRelated(to: event),
                let bubble = eventRoomBubbleDataMap[localEchoEvent]
            {
                // Important - Copy over user data! So for echo when resending for the "devicesAdded",
                // when we get here "event" is populated correctly, and the
                // "devices added" bubble will be create below (if needed).
                event.userData = localEchoEvent.userData
                bubble.events.removeAll { $0 == localEchoEvent }

                if bubble.events.isEmpty {
                    // Add this on to be deleted.
                    // NOTE: Delete inside the Main.async branch to keep table view consistency!
                    deletedBubbles.append(bubble)
                }
            }
        }


        var bubbles = [data]

        if let devicesAdded = event.userData?["devicesAdded"] as? [String: [String]] {
            // At the time this message was sent, the user had unknown devices. Checked if they still are.
            var devicesAddedFiltered = [String:[String]]()

            if let session = room.mxSession {
                for userId in devicesAdded.keys {
                    guard let userAddedDevices = devicesAdded[userId], !userAddedDevices.isEmpty else {
                        continue
                    }

                    if let userStoredDevices = session.crypto?.devices(forUser: userId) {
                        var stillUnknownDevices = [String]()

                        for deviceId in userAddedDevices {
                            guard let device = userStoredDevices[deviceId],
                               device.trustLevel?.localVerificationStatus != .unknown
                                && device.trustLevel?.localVerificationStatus != .unverified
                            else {
                                stillUnknownDevices.append(deviceId)

                                continue // Useless but needs to be there to satisfy the compiler.
                                // Logic would be more ugly the other way round.
                            }
                        }

                        if !stillUnknownDevices.isEmpty {
                            devicesAddedFiltered[userId] = stillUnknownDevices
                        }
                    }
                    else {
                        devicesAddedFiltered[userId] = userAddedDevices
                    }
                }
            }
            else {
                devicesAddedFiltered = devicesAdded
            }

            devicesAddedFiltered = devicesAddedFiltered.filter { userId, _ in
                if let member = roomState?.members.member(withUserId: userId) {
                    return member.membership == .join
                }

                return true
            }

            if !devicesAddedFiltered.isEmpty,
                let devicesAddedBubbleData = RoomBubbleData(self, state: roomState, events: [], type: RoomBubbleDataType.devicesAdded)
            {
                devicesAddedBubbleData.userData = ["devicesAdded": devicesAddedFiltered]
                bubbles.append(devicesAddedBubbleData)
                data.linkedBubbles = [devicesAddedBubbleData]
            }
        }

        DispatchQueue.main.async { [weak self] in
            // Don't add dupes.
            for bubble in bubbles {
                bubble.events.removeAll { self?.eventRoomBubbleDataMap[$0] != nil }
            }

            // Delete the bubbles and gather the index paths.
            for bubble in deletedBubbles {
                deletedIndexPaths.appendOptional(contentsOf: self?.deleteBubble(bubble: bubble, callDelegate: false))
            }

            var newIndexPaths = [IndexPath]()
            var changedIndexPaths = [IndexPath]()

            if self?.pagingDirection != nil {
                self?.addBubbles(bubbleData: bubbles, toArray: &self!.roomBubbleDataPaging,
                           direction: direction, newIndexPaths: &newIndexPaths,
                           changedIndexPaths: &changedIndexPaths)

                if !deletedIndexPaths.isEmpty {
                    self?.delegate?.didChange(
                        deleted: deletedIndexPaths, added: nil, changed: nil,
                        live: false, disableScroll: false)
                }
            }
            else {
                self?.addBubbles(bubbleData: bubbles, toArray: &self!.roomBubbleData,
                           direction: direction, newIndexPaths: &newIndexPaths,
                           changedIndexPaths: &changedIndexPaths)

                self?.delegate?.didChange(
                    deleted: deletedIndexPaths, added: newIndexPaths,
                    changed: changedIndexPaths, live: (direction == .forwards),
                    disableScroll: false)
            }
        }
    }
    
    private func addBubbles(bubbleData: [RoomBubbleData], toArray array: inout [RoomBubbleData],
                            direction: MXTimelineDirection, newIndexPaths: inout [IndexPath],
                            changedIndexPaths:inout [IndexPath])
    {
        guard !bubbleData.isEmpty else {
            return
        }

        var bubbles = bubbleData
        var nInserted = bubbles.count

        if direction == .backwards {
            if let last = bubbles.last {
                bubbles.removeLast()

                let oldEventCount = array.first?.events.count

                if !coalesceBubbles(existing: array.first, new: last, direction: direction) {
                    array.insert(last, at: 0)
                }
                else {
                    // Nothing inserted = the bubble data was coalesced!
                    nInserted -= 1

                    // Mark that index path (NOT where it will end up, please
                    // see tableView.reloadRows, the changed index paths are considered
                    // to be as they were before the update) as "changed"
                    if let oldEventCount = oldEventCount, oldEventCount != array.first?.events.count {
                        changedIndexPaths.append(IndexPath(row: reverseOrder ? (array.count - 1) : 0, section: sectionMessages))
                    }
                }
            }

            array.insert(contentsOf: bubbles, at: 0)

            for i in 0 ..< nInserted {
                newIndexPaths.append(IndexPath(row: reverseOrder ? (array.count - 1 - i) : i, section: sectionMessages))
            }
        }
        else {
            if let first = bubbles.first {
                bubbles.removeFirst()

                let oldEventCount = array.last?.events.count

                if !coalesceBubbles(existing: array.last, new: first, direction: direction) {
                    array.append(first)
                }
                else {
                    // Nothing inserted = the bubble data was coalesced!
                    nInserted -= 1

                    // If the number of events in coalesced bubble has changed
                    // (it may not have in subclasses that could filter events) mark the cell as changed.
                    if let oldEventCount = oldEventCount, oldEventCount != array.last?.events.count {
                        changedIndexPaths.append(IndexPath(row: reverseOrder ? 0 : (array.count - 1), section: sectionMessages))
                    }
                }
            }

            array.append(contentsOf: bubbles)

            for i in (array.count - nInserted) ..< array.count {
                newIndexPaths.append(IndexPath(row: reverseOrder ? (array.count - 1 - i) : i, section: sectionMessages))
            }
        }
    }
    
    /**
     Store the local echo event in the room. Also, stamp it with the current time, so we can later detect if we find events that seem "stuck" in sending.
     */
    public static func storeLocalEcho(room: MXRoom, localEcho: MXEvent?) {
        guard let event = localEcho else {
            return
        }

        if event.userData == nil {
            event.userData = [:]
        }

        event.userData?["echoAddedTs"] = Int64(Date().timeIntervalSince1970)

        room.storeOutgoingMessage(event)
    }
    
    public func insertLocalEcho(event: MXEvent?, roomState: MXRoomState) {
        guard let event = event else {
            return
        }

        RoomDataSource.storeLocalEcho(room: room, localEcho: event)
        onEvent(event, .forwards, roomState)
    }
    
    public var isPaging: Bool {
        return pagingDirection != nil
    }


    // MARK: Observers

    @objc
    func didChangeSentState(_ notification: Notification) {
        guard let event = notification.object as? MXEvent,
              event.roomId == room.roomId
        else {
            return
        }

        if let bubbleData = eventRoomBubbleDataMap[event],
            let indexPath = indexPath(for: bubbleData)
        {

            // Just failed to decrypt?!?
            if event.isEncrypted, event.decryptionError != nil {
                // Remove the bubble, we don't display these.
                self.deleteBubble(bubble: bubbleData)
                return
            }

            delegate?.didChange(
                deleted: nil, added: nil, changed: [indexPath],
                live: false, disableScroll: false)
        }

        // Did the event just fail to send?
        if event.isFailedWithUnknownDevices() {
            self.didFailWithUnknownDevices(event)
        }
    }

    @objc
    func didDecrypt(_ notification: Notification) {
        guard let event = notification.object as? MXEvent,
              event.roomId == room.roomId
        else {
            return
        }

        if let bubbleData = self.eventRoomBubbleDataMap[event],
            let indexPath = self.indexPath(for: bubbleData)
        {

            delegate?.didChange(
                deleted: nil, added: nil, changed: [indexPath],
                live: false, disableScroll: false)

            if let linkedBubbles = bubbleData.linkedBubbles {
                bubbleData.linkedBubbles = nil
                linkedBubbles.forEach { deleteBubble(bubble: $0) }
            }
        }
    }

    @objc
    func sessionDidSync(_ notification: Notification) {
        guard let session = notification.object as? MXSession,
              session == room.mxSession
        else {
            return
        }

        // Any unknown devices?
        checkForUnknownDevices()
    }


    // MARK: UITableViewDataSource
    /**
     We have three sections. In the first, optionally the "paging" header. In section 1 we have the actual data.
     */
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == sectionHeader {
            return isPaging ? 1 : 0
        }

        return roomBubbleData.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        switch (indexPath.section) {
        case sectionHeader:
            if let cell = delegate?.createCell(type:.paging, at: indexPath) {
                return cell
            }

        case sectionMessages:
            if let bubble = roomBubbleData(at: indexPath), let cell = delegate?.createCell(at: indexPath, for: bubble) {
                return cell
            }

        default:
            break
        }

        return UITableViewCell() // Fallback to avoid crash.
    }
    
    //MARK: UITableViewDelegate
    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        guard let bubble = roomBubbleData(at: indexPath), bubble.isIncoming else {
            return
        }

        acknowledgeEvents(events: bubble.events)
    }
    
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        delegate?.didScroll(offset: scrollView.contentOffset.y)
    }
    
    open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }


    //MARK: UICollectionViewDataSource

    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == sectionHeader {
            return 0
        }

        return roomBubbleData.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let bubble = roomBubbleData(at: indexPath),
            let cell = delegate?.createCollectionViewCell(at: indexPath, for: bubble)
        {
            acknowledgeEvents(events: bubble.events)
            return cell
        }

        return UICollectionViewCell() // Fallback to avoid crash
    }
    
    //MARK: Bubble methods
    open func roomBubbleData(at indexPath: IndexPath) -> RoomBubbleData? {
        if indexPath.section == sectionMessages,
            indexPath.row >= 0,
            indexPath.row < roomBubbleData.count
        {
            return roomBubbleData[reverseOrder ? (roomBubbleData.count - 1 - indexPath.row) : indexPath.row]
        }

        return nil
    }
    
    public func roomBubbleData(before bubble: RoomBubbleData) -> RoomBubbleData? {
        if let index = roomBubbleData.firstIndex(of: bubble), index > 0 {
            return roomBubbleData[index - 1]
        }

        return nil
    }
    
    open func indexPath(for bubble: RoomBubbleData) -> IndexPath? {
        if let index = roomBubbleData.firstIndex(of: bubble) {
            return IndexPath(row: reverseOrder ? (roomBubbleData.count - 1 - index) : index, section: sectionMessages)
        }

        return nil
    }
    
    public var canPageBackwards: Bool {
        return !backPagingDisabled && (timeline?.canPaginate(.backwards) ?? false)
    }
    
    public var canPageForwards: Bool {
        return timeline?.canPaginate(.forwards) ?? false
    }
    
    private func page(direction: MXTimelineDirection, onlyFromStore: Bool)
    {
        guard !isPaging, timeline?.canPaginate(direction) ?? false else {
            return
        }

        pagingDirection = direction

        delegate?.pagingStarted(direction: direction)

        queue.async { [weak self] in
            self?.timeline?.paginate(self?.pagingSize ?? 0, direction: direction,
                               onlyFromStore: onlyFromStore) { response in
                DispatchQueue.main.async {
                    self?.pagingDone(direction, success: response.isSuccess, scrollToEventId: nil)
                    self?.updateReadReceipts(eventIds: nil)
                }
            }
        }
    }
    
    /**
     Try to coalesce an existing cell with new bubble data.

     For some cells, like when room members are joining/leaving, we coalesce multiple MX events into the
     same Table view UI cell, displaying it like "x members joined, y members left".
     This function checked if we can join the two bubbles together by appending/prepending the events of one into the other.
     
     This function will also update the mapping between `MXEvent` objects and the `RoomBubbleData` objects displaying them.
     
     - Parameter existing: The bubble data for an existing cell (or nil, in which case the function returns nil).
     - Parameter new: The new bubble data.
     - Parameter direction: The direction, determines if we should append or prepend events when coalescing.
     - Returns: true if the two bubbles where joined, false otherwise.
     - Warning: This func has the side effect of updating the event mapping from event -> RoomBubbleData!
     */
    open func coalesceBubbles(existing: RoomBubbleData?, new: RoomBubbleData, direction: MXTimelineDirection) -> Bool {
        if let existing = existing, existing.bubbleType == .newMember, new.bubbleType == .newMember {
            // Coalesce the events!
            if direction == .backwards {
                existing.events.insert(contentsOf: new.events, at: 0)
            }
            else {
                existing.events.append(contentsOf: new.events)
            }

            new.events.forEach { eventRoomBubbleDataMap[$0] = existing }

            return true
        }
        new.events.forEach { eventRoomBubbleDataMap[$0] = new }

        return false
    }
    
    open func pagingDone(_ direction: MXTimelineDirection, success: Bool, scrollToEventId: String?) {
        var newIndexPaths = [IndexPath]()
        var changedIndexPaths = [IndexPath]()

        if !roomBubbleDataPaging.isEmpty {
            if direction == .backwards,
                roomBubbleData.count > 0
            {
                changedIndexPaths.append(IndexPath(row: reverseOrder ? (roomBubbleData.count - 1) : 0, section: sectionMessages))
            }

            addBubbles(bubbleData: roomBubbleDataPaging, toArray: &roomBubbleData,
                       direction: direction, newIndexPaths: &newIndexPaths,
                       changedIndexPaths: &changedIndexPaths)
        }

        roomBubbleDataPaging = []
        pagingDirection = nil

        var scrollToIndexPath: IndexPath? = nil

        if let scrollToEventId = scrollToEventId,
           let scrollToEventEntry = eventRoomBubbleDataMap.first(where: { $0.key.eventId == scrollToEventId })
        {
            scrollToIndexPath = indexPath(for: scrollToEventEntry.value)

            if var scrollTo = scrollToIndexPath {
                // We have an index path for the "last read" event.
                // Now, this is only for incoming events, so make sure to
                // "walk up" from this event to all the ones we might have
                // sent ourselves after this point in time.
                scrollTo = IndexPath(row: scrollTo.row, section: scrollTo.section)

                while
                    let nextBubble = roomBubbleData(at: IndexPath(row: scrollTo.row + 1, section: scrollTo.section)),
                    !nextBubble.isIncoming
                {
                    scrollTo.row += 1
                }

                scrollToIndexPath = scrollTo
            }
        }

        delegate?.pagingDone(direction: direction, newIndexPaths: newIndexPaths,
                             changedIndexPaths: changedIndexPaths, success: success,
                             scrollToIndexPath: scrollToIndexPath)
    }
    
    public func pageBackwards() {
        page(direction: .backwards, onlyFromStore: false)
    }
    
    public func pageForwards() {
        page(direction: .forwards, onlyFromStore: false)
    }
    
    /**
     Delete an outgoing event. This means remove it from its containing bubble (removing the bubble altogether if it is left empty) and calling room.
     */
    func deleteOutgoingEvent(event: MXEvent) {
        guard let bubble = eventRoomBubbleDataMap[event] else {
            return
        }

        eventRoomBubbleDataMap.removeValue(forKey: event)

        bubble.events.removeAll { $0.eventId == event.eventId }

        room.removeOutgoingMessage(event.eventId)

        if bubble.events.isEmpty {
            deleteBubble(bubble: bubble)
        }
    }

    @discardableResult
    func deleteBubble(bubble:RoomBubbleData, callDelegate: Bool = true) -> [IndexPath]?
    {
        var removedIndexPaths = [IndexPath]()
        
        var bubblesToDelete = [bubble]

        if let linkedBubbles = bubble.linkedBubbles {
            //TODO - currenly only 1 level. Should this be recursive instead
            // allowing arbitrary depths?
            bubblesToDelete.append(contentsOf: linkedBubbles)
        }
        
        // First get all affected IndexPaths.
        bubblesToDelete.forEach {
            if let indexPath = indexPath(for: $0) {
                removedIndexPaths.append(indexPath)
            }
        }
        
        // Then delete the bubbles from the array.
        bubblesToDelete.forEach { bubble in
            bubble.events.forEach { eventRoomBubbleDataMap.removeValue(forKey: $0) }

            if let index = roomBubbleData.firstIndex(of: bubble) {
                roomBubbleData.remove(at: index)
            }
        }
        
        if callDelegate {
            delegate?.didChange(deleted: removedIndexPaths, added: nil,
                                changed: nil, live: false, disableScroll: false)
        }
        
        //TODO - Check if we can coalesce the bubble before and after this one.
        
        return removedIndexPaths.isEmpty ? nil : removedIndexPaths
    }
    
    /**
     Delete all bubbles of the given type.
     */
    func deleteAllBubbles(ofType type: RoomBubbleDataType) {
        roomBubbleData.filter { $0.bubbleType == type }.forEach { deleteBubble(bubble: $0) }
    }
    
    /**
     Go through all failed messages, resending them.
     */
    func resendAllFailedMessages() {
        if let failedMessages = room.outgoingMessages()?.filter({ $0.sentState == MXEventSentStateFailed }) {
            resendMessage(inArray: failedMessages, atIndex: 0)
        }
    }
    
    /**
     Resend a given message. If the message is a failed one, try to resend. Otherwise create a copy of the message and send that.
     */
    func resendMessage(event: MXEvent) {
        resendMessage(inArray: [event], atIndex: 0)
    }

    /**
     Redact a given message.
     */
    func redactMessage(event: MXEvent) {
        room.redactEvent(event.eventId, reason: nil) { response in
            // Ignored for now
        }
    }

    
    func cancelMessage(event: MXEvent) {
        if event.isLocalEvent() {
            room.cancelSendingOperation(event.eventId)
        }

        deleteOutgoingEvent(event: event)
    }
    
    /**
     Recursive function to send failed messages.
     */
    private func resendMessage(inArray array: [MXEvent], atIndex index: Int) {
        guard index < array.count else {
            return
        }

        let event = array[index]

        // If the event was failed, remove it! We'll add a new local echo.
        if event.sentState == MXEventSentStateFailed {
            deleteOutgoingEvent(event: event)
        }
        
        var localEcho: MXEvent?
        
        room.sendMessage(withContent: event.content, localEcho: &localEcho) { [weak self] response in
            self?.resendMessage(inArray: array, atIndex: index + 1)
        }

        // Copy user data properties over to new event object
        localEcho?.userData = event.userData

        // Add the local echo!
        insertLocalEcho(event: localEcho, roomState: room.dangerousSyncState)
    }
    
    /**
     Called for events that failed to send because there are unknown devices present in the room.

     This function is called on the main thread. Default implementation sets the devices to "known" and resend the message.
     */
    open func didFailWithUnknownDevices(_ event: MXEvent) {
        guard let devices = event.sentFailedUnknownDevices,
              let session = room.mxSession
        else {
            return
        }

        // Implement our "Warn Only if Devices added" (WOIDA) scheme.
        //
        // We start by getting the truly new devices, i.e. if this is the first
        // time EVER that we see devices for a user, they are considered "known".
        var devicesAdded = [String: [String]]()

        for userId in devices.userIds() {
            if !(session.crypto?.devices(forUser: userId).isEmpty ?? true),
                let userDevices = devices.deviceIds(forUser: userId)
            {
                devicesAdded[userId] = userDevices
            }
        }

        // Unknown devices now contain "devices added".
        // Store in user property of event.
        if event.userData == nil {
            event.userData = [:]
        }

        event.userData?["devicesAdded"] = devicesAdded

        (session.crypto as? MXLegacyCrypto)?.setDevicesKnown(devices) {
            self.resendMessage(event: event)
        }
    }
    
    /**
     Checks the device lists for all joined members if we have any devices in state "unknown".
     */
    func checkForUnknownDevices() {
        // This seems to cause dead locks somehow.
//        guard
//            room.summary.isEncrypted,
//            let session = room.mxSession else {return}
//
//        room.members { (response) in
//            if response.isSuccess, let members = response.value {
//                if let joined = members?.joinedMembers {
//                    let memberIds = joined.compactMap({ $0.userId })
//                    session.crypto.downloadKeys(memberIds, forceDownload: false, success: { (usersDevicesMap) in
//                        guard let usersDevicesMap = usersDevicesMap else { return }
//
//                        var unknownDeviceUsers = 0
//                        var unknownDevices = 0
//                        var unverifiedDevices = 0
//
//                        for userId in usersDevicesMap.userIds() ?? [] {
//                            guard userId != session.myUser.userId else { continue }
//                            let tempUnknown = unknownDevices
//                            for deviceId in usersDevicesMap.deviceIds(forUser: userId) ?? [] {
//                                if let device = usersDevicesMap.object(forDevice: deviceId, forUser: userId) {
//                                    if device.verified == MXDeviceUnverified {
//                                        unverifiedDevices += 1
//                                    } else if device.verified == MXDeviceUnknown {
//                                        unknownDevices += 1
//                                    }
//                                }
//                            }
//                            if tempUnknown != unknownDevices {
//                                // Changed, so add +1 for this user
//                                unknownDeviceUsers += 1
//                            }
//                        }
//                        self.delegate?.unknownDevicesUpdate(unknownDevices: unknownDevices, unknownDeviceUsers: unknownDeviceUsers)
//                    }, failure: { (error) in
//
//                    })
//                }
//            }
//        }
    }

    /**
     Handle read receipts.

     A "bubble" is considered to be "received" if it is not incoming and if any of the events it contains has
     received a receipt from ANY other user than the current.

     Since bubbles are storted in time, if we find that a certain bubble is "received", then we just assume all
     other bubbles before it are received, and mark them as such.

     - parameter eventIds: Optional array of event ids that are received. We set this if we get a `m.receipt` event.
     */
    open func updateReadReceipts(eventIds: [String]?) {
        DispatchQueue.main.async { [weak self] in
            guard let store = self?.room.mxSession?.store,
                  let members = self?.room.dangerousSyncState.members
            else {
                return
            }

            // Get the last timestamp of any event that any member of the room has read (exclude ourselves!)
            var maxTsRead: UInt64 = 0

            for member in members.joinedMembers {
                guard member.userId != self?.room.myUser?.userId,
                      let roomId = self?.room.roomId
                else {
                    continue
                }

                let receiptData = store.getReceiptInRoom(roomId, threadId: kMXEventTimelineMain, forUserId: member.userId)

                if let eventId = receiptData?.eventId,
                   let event = store.event(withEventId: eventId, inRoom: roomId)
                {
                    maxTsRead = max(maxTsRead, event.originServerTs)
                }
            }

            // Get index of first bubble that is marked as not received. All others we can ignore.
            if let unreceivedBubble = self?.roomBubbleData.first(where: { !$0.isIncoming && !$0.isReceived } ),
               let index = self?.roomBubbleData.firstIndex(of: unreceivedBubble)
            {
                var indexOfLastReceived = 0

                // Walk backwards though the bubbles, finding the first (i.e. last) one that
                // contains an event anyone has read (= that has an earlier or same timestamp).
                for idxBubble in (index ..< (self?.roomBubbleData.count ?? 0)).reversed() {
                    let bubble = self?.roomBubbleData[idxBubble]

                    guard !(bubble?.isIncoming ?? true) else {
                        continue
                    }

                    if bubble?.events.contains(where: { $0.originServerTs <= maxTsRead }) ?? false {
                        indexOfLastReceived = idxBubble
                        break
                    }
                }

                var changedIndexPaths = [IndexPath]()

                if indexOfLastReceived >= index {
                    for i in index ... indexOfLastReceived {
                        guard let b = self?.roomBubbleData[i] else {
                            continue
                        }

                        if !b.isIncoming, !b.isReceived {
                            b.isReceived = true
                            changedIndexPaths.appendOptional(self?.indexPath(for: b))
                        }
                    }
                }

                if !changedIndexPaths.isEmpty {
                    self?.delegate?.didChange(
                        deleted: nil, added: nil, changed: changedIndexPaths,
                        live: false, disableScroll: true)
                }
            }
        }
    }

    /**
     Called when we are actually displaying a bubble containing events.

     These events are now considered "read", so we acknowledge them (and also update the read marker if needed).
     */
    private func acknowledgeEvents(events: [MXEvent]) {
        // Get TS of read marker (if found)
        if tsLastRead == 0 {
            if let readMarkerEventId = room.accountData.readMarkerEventId,
               let readMarkerEvent = room.mxSession.store.event(withEventId: readMarkerEventId, inRoom: room.roomId) ?? eventRoomBubbleDataMap.first(where: { $0.key.eventId == readMarkerEventId })?.key
            {
                tsLastRead = readMarkerEvent.originServerTs
            }
        }
        
        // Acknowledge the events
        for event in events {
            var updateReadMarker = false
            
            // If the event has a later TS than our previous RM, update that as well.
            //
            if tsLastRead > 0, event.originServerTs >= tsLastRead {
                tsLastRead = event.originServerTs
                updateReadMarker = true
            }

            room.acknowledgeEvent(event, andUpdateReadMarker: updateReadMarker)
        }
    }
}
