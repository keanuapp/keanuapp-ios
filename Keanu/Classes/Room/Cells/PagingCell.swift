//
//  PagingCell.swift
//  Keanu
//
//  Created by N-Pex on 22.05.19.
//

import UIKit
import MatrixSDK

/**
 A cell used to show "Loading more messages" status.
 */
open class PagingCell: UITableViewCell {
    
    open class var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }
    
    public class var defaultReuseId: String {
        return String(describing: self)
    }
}


