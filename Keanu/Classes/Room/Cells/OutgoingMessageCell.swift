//
//  OutgoingMessageCell.swift
//  Keanu
//
//  Created by N-Pex on 05.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit

open class OutgoingMessageCell: MessageCell {

    public override func awakeFromNib() {
        super.awakeFromNib()

        // This is the same as tapping the avatar image.
        let tap = UITapGestureRecognizer(target: self, action: #selector(didTapContextMenu))
        tap.cancelsTouchesInView = false

        statusLabel.addGestureRecognizer(tap)
        statusLabel.isUserInteractionEnabled = true
    }
}
