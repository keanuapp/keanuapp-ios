//
//  TypingIndicator.swift
//  Keanu
//
//  Created by N-Pex on 15.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit
import MatrixSDK
import KeanuCore

open class TypingIndicator: UIView {
    open class var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }

    @IBOutlet open weak var label:UILabel!
    
    open func populate(_ room:MXRoom) {
        var typingUsers = room.typingUsers
        
        // Remove ourselves from typers
        typingUsers?.removeAll(where: { $0 == room.myUser?.userId })
        
        guard let typers = typingUsers, typers.count > 0 else {
            isHidden = true
            label.text = ""
            return
        }
        
        if typers.count == 1 {
            // Special case, if only one typer we switch to the display name
            var typerString = ""
            if let state = room.dangerousSyncState, let typer = typers.first, let member = state.members.member(withUserId: typer) {
                typerString = member.displayname ?? member.userId ?? ""
            }
            // Typing indicator when one is typing.
            label.text = "% is typing".localize(value: typerString)
        } else {
            // Typing indicator when several are typing.
            label.text = "% people are typing".localize(value: Formatters.format(int: typers.count))
        }
        isHidden = false
    }
}
