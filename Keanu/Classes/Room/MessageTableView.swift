//
//  MessageTableView.swift
//  Keanu
//
//  Created by N-Pex on 07.01.20.
//

/**
 A UITableView that handles dynamic updates via the "didChange" callback.
 */
import UIKit

open class MessageTableView: UITableView {
    
    public var latestAtTop = false {
        didSet(value) {
            // If set to reverse order, make sure to initialize these differently
            viewingFirstMessage = !value
            viewingLastMessage = value
        }
    }
    public var isUpdating = false
    var viewingFirstMessage = true
    var viewingLastMessage = false
    
    private func updateTableView(deleted: [IndexPath]?, added: [IndexPath]?, changed: [IndexPath]?, live: Bool) {
        UITableView.setAnimationsEnabled(false)
        beginUpdates()

        if let deleted = deleted, deleted.count > 0 {
            self.deleteRows(at: deleted, with: .none)
        }

        if let changed = changed, changed.count > 0 {
            self.reloadRows(at: changed, with: .none)
        }

        if let added = added, added.count > 0 {
            self.insertRows(at: added, with: (live ? .bottom : .none))
        }

        endUpdates()
        UITableView.setAnimationsEnabled(true)
    }
    
    open func didChange(deleted: [IndexPath]?, added: [IndexPath]?, changed: [IndexPath]?, live: Bool, disableScroll: Bool)
    {
        let scrollToTop = viewingFirstMessage || !viewportIsFilled
        let scrollToBottom = viewingLastMessage || !viewportIsFilled

        isUpdating = true
        showsVerticalScrollIndicator = false
        let oldScrollEnabled = isScrollEnabled
        isScrollEnabled = false
        
        let oldOffset = contentOffset

        if let firstDeleted = deleted?.first,
            let firstAdded = added?.first,
            firstDeleted == firstAdded,
            (added?.count ?? 0) == 1,
            (deleted?.count ?? 0) == 1,
            (changed?.count ?? 0) == 0
        {
            // Actually a change, not a remove/add.
            updateTableView(deleted: nil, added: nil, changed: [firstAdded], live: live)
        } else {
            updateTableView(deleted: deleted, added: added, changed: changed, live: live)
        }
        
        setNeedsLayout()
        layoutIfNeeded()

        if !disableScroll && live == latestAtTop {
            // TODO: #rectForRow triggers a crash later in UITextView#setTextInsets, when the cell needs to be
            //       created and it is a cell which defines these. (Currently Incoming- and OutgoingMessageCell.
            //       Can we avoid this somehow?

            let addedCellsHeight = added?.reduce(0, { $0 + rectForRow(at: $1).height }) ?? 0

            let deletedCellsHeight = deleted?.reduce(0, { $0 + rectForRow(at: $1).height }) ?? 0

            let offset = added?.count ?? 0
            let changedCellsNewHeight = changed?.reduce(0, { $0 + rectForRow(at: IndexPath(row: $1.row + offset, section: $1.section)).height }) ?? 0

            // Store the old heights for the cells.
            let changedCellsOldHeight = changed?.reduce(0, { $0 + rectForRow(at: $1).height }) ?? 0

            let delta = addedCellsHeight - deletedCellsHeight + changedCellsNewHeight - changedCellsOldHeight

            if delta != 0 {
                setContentOffset(CGPoint(x: oldOffset.x, y: oldOffset.y + delta), animated: false)
            }
        }
        
        showsVerticalScrollIndicator = true
        isScrollEnabled = oldScrollEnabled
        isUpdating = false
        
        guard !disableScroll else {
            return
        }

        if latestAtTop {
            if scrollToTop, let first = firstIndexPath {
                scrollToRow(at: first, at: .bottom, animated: live)
            }
        }
        else {
            guard scrollToBottom else {
                return
            }

            if !viewportIsFilled {
                setContentOffset(.zero, animated: false)
            }
            else if let last = lastIndexPath {
                DispatchQueue.main.async {
                    self.scrollToRow(at: last, at: .top, animated: live)
                }
            }
        }
    }

    public func didScroll() {
        if !isUpdating {
            let scrolledToTop = contentOffset.y == 0
            let scrolledToBottom = contentOffset.y >= (contentSize.height - frame.height)

            viewingFirstMessage = scrolledToTop
            viewingLastMessage = scrolledToBottom
        }
    }
}
