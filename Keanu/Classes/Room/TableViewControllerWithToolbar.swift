//
//  TableViewControllerWithToolbar.swift
//  Keanu
//
//  Created by N-Pex on 02.10.18.
//  Copyright © 2018 Guardian Project. All rights reserved.
//

import UIKit
import PureLayout

/**
 A base UITableViewController that listens for keyboard events, and that (optionally) displays a toolbar on top of the keyboard view.
 */
open class TableViewControllerWithToolbar: UITableViewController {
    private var tableViewBottomLayoutConstraint: NSLayoutConstraint?

    private var originalTableView: UITableView?
    override open var tableView: UITableView! {
        get {
            if originalTableView == nil {
                // View not created yet, create it now
                _ = super.tableView
            }
            return originalTableView
        }
        set {
            originalTableView = newValue
        }
    }
    
    override open func viewDidLoad() {
        super.viewDidLoad()

        originalTableView = super.tableView

        let view = UIView()
        view.backgroundColor = .keanuBackground
        view.frame = tableView.frame
        self.view = view
        view.addSubview(tableView)

        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.autoPinEdgesToSuperviewSafeArea(with: .zero, excludingEdge: .bottom)
        tableViewBottomLayoutConstraint = tableView.autoPinEdge(toSuperviewSafeArea: .bottom)
    }
    
    open var toolbarView: UIView? {
        willSet {
            toolbarView?.removeFromSuperview()
        }
        didSet {
            // Make sure view is created.
            _ = view

            // Remove whatever the tableView bottom was pinned to.
            tableViewBottomLayoutConstraint?.autoRemove()

            if let toolbar = toolbarView {
                view.addSubview(toolbar)

                toolbar.autoPinEdge(toSuperviewEdge: .bottom) //.autoPinEdge(toSuperviewSafeArea: .bottom)
                toolbar.autoPinEdge(.leading, to: .leading, of: self.view, withOffset: 0)
                toolbar.autoPinEdge(.trailing, to: .trailing, of: self.view, withOffset: 0)
                tableViewBottomLayoutConstraint = tableView.autoPinEdge(.bottom, to: .top, of: toolbar, withOffset: 0)
            }
            else {
                // Remove toolbar.
                tableViewBottomLayoutConstraint = tableView.autoPinEdge(.bottom, to: .bottom, of: self.view, withOffset: 0)
            }
        }
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        // Keyboard handling from here:
        // https://stackoverflow.com/questions/43332308/ios-tableview-controller-keyboard-offset-example-project-inside
        NotificationCenter.default.addObserver(
            self, selector: #selector(keyboardWillShow(_:)),
            name: UIResponder.keyboardWillShowNotification, object: nil)

        NotificationCenter.default.addObserver(
            self, selector: #selector(keyboardWillHide(_:)),
            name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override open func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Remove the observers so the code won't be called all the time.
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(_ notification: NSNotification) {
        updateBottomLayoutConstraintWithNotification(notification, show: true)
    }
    
    @objc func keyboardWillHide(_ notification: NSNotification) {
        updateBottomLayoutConstraintWithNotification(notification, show: false)
    }
    
    func updateBottomLayoutConstraintWithNotification(_ notification: NSNotification, show: Bool) {
        guard let userInfo = notification.userInfo else {
            return
        }
        
        // get data from the userInfo
        let animationDuration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
        let keyboardEndFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue ?? .zero
        let rawAnimationCurve = ((userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber)?.uint32Value ?? 0) << 16
        let animationCurve = UIView.AnimationOptions(rawValue: UInt(rawAnimationCurve))
        
        let newValue = show ? keyboardEndFrame.height : 0

        // Animate the changes.
        UIView.animate(withDuration: animationDuration, delay: 0, options:
            [.beginFromCurrentState, animationCurve], animations:
            { [weak self] in
                self?.view.layoutIfNeeded()
            
                // Just translate views away from the keyboard.
                self?.toolbarView?.transform = CGAffineTransform(translationX: 0, y: -newValue)
                self?.tableView.transform = CGAffineTransform(translationX: 0, y: -newValue)
            })
    }
    
    // MARK: Actions
    
    /**
     Dismisses ourselves, animated.
     Handles being in a `UINavigationController` gracefully.
     
     Can be a callback for simple "Cancel", "Done" etc. buttons.
     */
    @IBAction func dismiss() {
        if let nav = navigationController {
            nav.dismiss(animated: true)
        }
        else {
            dismiss(animated: true)
        }
    }
}
