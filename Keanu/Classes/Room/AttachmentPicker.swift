//
//  AttachmentPicker.swift
//  Keanu
//
//  Created by N-Pex on 15.10.18.
//  Copyright © 2018 GuardianProject. All rights reserved.
//

import UIKit
import MatrixSDK

open class AttachmentPicker: UIView {

    open class func createNew(forView: UIView) -> AttachmentPicker {
        let attachmentPicker = AttachmentPicker.nib.instantiate(withOwner: nil, options: nil)[0] as! AttachmentPicker
        attachmentPicker.translatesAutoresizingMaskIntoConstraints = false
        attachmentPicker.forView = forView
        attachmentPicker.height = attachmentPicker.autoSetDimension(.height, toSize: 0)
        attachmentPicker.closeTapRecognizer = UITapGestureRecognizer(target: attachmentPicker, action: #selector(closeAttachmentPicker))
        if let recognizer = attachmentPicker.closeTapRecognizer {
            recognizer.isEnabled = false
            forView.addGestureRecognizer(recognizer)
        }
        return attachmentPicker
    }
    
    open class var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }

    public var defaultHeight: CGFloat = 100
    
    private var forView: UIView?
    private var closeTapRecognizer: UITapGestureRecognizer?
    private var height: NSLayoutConstraint?
    
    @IBOutlet open weak var actionsStackView: UIStackView!

    /**
     We create a prototype for action buttons in IB and bind it here. Then, when adding actions,
     we create copies of this prototype, so that all actions get the same look.

     Note: don't make this weak, or we'll lose it on clearActions()!
     */
    @IBOutlet open var actionButtonPrototype: UIButton!
    
    private var buttonActionMap: [UIControl: (() -> Void)] = [:]
    
    open override func awakeFromNib() {
        super.awakeFromNib()

        defaultHeight = frame.size.height
        clearActions() // Remove design time stuff.
    }
    
    open func clearActions() {
        actionsStackView.subviews.forEach { $0.removeFromSuperview() }
        buttonActionMap.removeAll()
    }
    
    open func actionCount() -> Int {
        return actionsStackView.arrangedSubviews.count
    }
    
    open var actions: [UIButton] {
        return actionsStackView.arrangedSubviews.compactMap { $0 as? UIButton }
    }
    
    open func removeAction(withId id: String) {
        if let button = actions.first(where: { id == $0.accessibilityIdentifier }) {
            button.removeFromSuperview()
            buttonActionMap.removeValue(forKey: button)
        }
    }

    @discardableResult
    open func addAction(_ label: String, id: String? = nil, at: Int? = nil, _ callback: @escaping (() -> Void)) -> UIButton? {
        guard !label.isEmpty,
            let actionButtonPrototype = actionButtonPrototype
        else {
            return nil
        }

        let button = actionButtonPrototype.copyView() as UIButton
        
        button.titleLabel?.font = actionButtonPrototype.titleLabel?.font
        button.layer.cornerRadius = actionButtonPrototype.layer.cornerRadius
        button.layer.borderColor = actionButtonPrototype.layer.borderColor
        button.layer.borderWidth = actionButtonPrototype.layer.borderWidth
        
        buttonActionMap[button] = callback

        button.setTitle(label, for: .normal)
        button.addTarget(self, action: #selector(actionTapped(sender:)), for:.touchDown)

        if let at = at {
            actionsStackView.insertArrangedSubview(button, at: at)
        }
        else {
            actionsStackView.addArrangedSubview(button)
        }

        button.accessibilityIdentifier = id

        return button
    }
    
    @objc open func actionTapped(sender: UIControl) {
        buttonActionMap[sender]?()
    }
    
    public func openAttachmentPicker(anchor: UIView) {
        guard let view = forView else { return }
        if superview == nil {
            view.addSubview(self)
        }
        autoPinEdge(.leading, to: .leading, of: anchor)
        autoPinEdge(.trailing, to: .trailing, of: anchor)
        autoPinEdge(.bottom, to: .top, of: anchor)
        view.layoutIfNeeded()

        self.height?.constant = defaultHeight

        UIView.animate(withDuration: 0.3, animations: {
            view.layoutIfNeeded()
        })
        closeTapRecognizer?.isEnabled = true
    }
    
    @objc public func closeAttachmentPicker() {
        height?.constant = 0
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            self?.superview?.layoutIfNeeded()
        }) { [weak self] _ in
            self?.closeTapRecognizer?.isEnabled = false
            self?.removeFromSuperview()
        }
    }
}
