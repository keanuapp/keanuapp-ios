//
//  ContextMenuView.swift
//  Keanu
//
//  Created by Benjamin Erhart on 26.01.22.
//

import UIKit
import KeanuCore
import MobileCoreServices
import ISEmojiView

open class ContextMenuView: UIView, UIGestureRecognizerDelegate {

    public enum Action {
        case reaction(emoji: String)
        case reply
        case forward
        case resend
        case share
        case delete
        case viewProfile
    }

    private var callback: ((_ action: Action) -> Void)? = nil

    private weak var roomBubbleData: RoomBubbleData?

    private weak var room: MXRoom?

    open var menuBottomConstraint: NSLayoutConstraint?

    open var closeTapRecognizer: UITapGestureRecognizer?

    open lazy var menu: UIView = {
        let view = UIView()
        view.backgroundColor = .keanuBackground
        view.translatesAutoresizingMaskIntoConstraints = false

        view.addSubview(name)
        name.autoPinEdge(toSuperviewEdge: .top, withInset: 8)
        name.autoPinEdge(toSuperviewEdge: .leading, withInset: 8)

        view.addSubview(timestamp)
        timestamp.autoPinEdge(toSuperviewEdge: .trailing, withInset: 8)
        timestamp.autoPinEdge(.leading, to: .trailing, of: name, withOffset: 8)
        timestamp.centerYAnchor.constraint(equalTo: name.centerYAnchor).isActive = true

        view.addSubview(content)
        content.autoPinEdge(.top, to: .bottom, of: name, withOffset: 8)
        content.autoPinEdge(toSuperviewEdge: .leading, withInset: 8)
        content.autoPinEdge(toSuperviewEdge: .trailing, withInset: 8)

        var border = UIView()
        border.translatesAutoresizingMaskIntoConstraints = false
        border.backgroundColor = .keanuGray
        border.autoSetDimension(.height, toSize: 1)

        view.addSubview(border)
        border.autoPinEdge(.top, to: .bottom, of: content, withOffset: 8)
        border.autoPinEdge(toSuperviewEdge: .leading)
        border.autoPinEdge(toSuperviewEdge: .trailing)

        view.addSubview(fastQr)
        fastQr.autoPinEdge(.top, to: .bottom, of: border)
        fastQr.autoPinEdge(toSuperviewEdge: .leading)
        fastQr.autoPinEdge(toSuperviewEdge: .trailing)

        border = UIView()
        border.translatesAutoresizingMaskIntoConstraints = false
        border.backgroundColor = .keanuGray
        border.autoSetDimension(.height, toSize: 1)

        view.addSubview(border)
        border.autoPinEdge(.top, to: .bottom, of: fastQr)
        border.autoPinEdge(toSuperviewEdge: .leading)
        border.autoPinEdge(toSuperviewEdge: .trailing)

        var buttons = [quickReactionBt]
        if room?.canReply(to: roomBubbleData?.event) ?? false {
            buttons.append(replyBt)
        }
        if roomBubbleData?.event?.sentState == MXEventSentStateSent {
            buttons.append(forwardBt)
        }
        if roomBubbleData?.senderId == room?.mxSession?.myUserId {
            buttons.append(resendBt)
        }
        buttons.append(contentsOf: [copyBt, shareBt, deleteBt, viewProfileBt])

        var previous: UIView = border

        for button in buttons {
            view.addSubview(button)

            button.autoPinEdge(toSuperviewEdge: .leading, withInset: 8)
            button.autoPinEdge(toSuperviewEdge: .trailing, withInset: 8)

            button.autoPinEdge(.top, to: .bottom, of: previous, withOffset: 8)

            button.addTarget(self, action: #selector(doAction), for: .touchUpInside)

            previous = button
        }

        previous.autoPinEdge(toSuperviewSafeArea: .bottom, withInset: 8)

        return view
    }()

    open lazy var name: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font = .boldSystemFont(ofSize: view.font.pointSize)

        return view
    }()

    open lazy var timestamp: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textAlignment = .right
        view.font = .systemFont(ofSize: view.font.pointSize * 0.75)
        view.textColor = .keanuGray

        return view
    }()

    open lazy var content: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.numberOfLines = 0

        return view
    }()

    open lazy var fastQr: UIScrollView = {
        let view = UIScrollView()
        view.isDirectionalLockEnabled = true
        view.translatesAutoresizingMaskIntoConstraints = false

        var emojis = NSMutableOrderedSet()

        // Get the most recently used emojis.
        emojis.addObjects(from: EmojiLoader.recentEmojiCategory().emojis.map({ $0.selectedEmoji ?? $0.emoji }))

        // Fill up with emojis from the "Smileys and People" category up to 16.
        if emojis.count < 16 {
            emojis.addObjects(from: EmojiLoader.emojiCategories().first(where: { $0.category == .smileysAndPeople })?
                                .emojis.map({ $0.selectedEmoji ?? $0.emoji }) ?? [String]())

            if emojis.count > 16 {
                emojis.removeObjects(in: NSMakeRange(16, emojis.count - 16))
            }
        }

        var previous: UIView?

        for emoji in emojis {
            let bt = UIButton()
            bt.setTitle(emoji as? String)
            bt.translatesAutoresizingMaskIntoConstraints = false

            bt.addTarget(self, action: #selector(quickReaction), for: .touchUpInside)

            view.addSubview(bt)
            bt.autoPinEdge(toSuperviewEdge: .top, withInset: 8)
            bt.autoPinEdge(toSuperviewEdge: .bottom, withInset: 8)
            bt.autoMatch(.height, to: .height, of: view, withOffset: -16)

            if let previous = previous {
                bt.autoPinEdge(.leading, to: .trailing, of: previous, withOffset: 8)
            }
            else {
                bt.autoPinEdge(toSuperviewEdge: .leading, withInset: 8)
            }

            previous = bt
        }

        previous?.autoPinEdge(toSuperviewEdge: .trailing, withInset: 8)

        return view
    }()

    open lazy var quickReactionBt = MenuButton(.add_reaction, "Quick Reaction".localize())

    open lazy var replyBt = MenuButton(.reply, "Reply".localize())

    open lazy var forwardBt = MenuButton(.forward, "Forward".localize())

    open lazy var resendBt = MenuButton(.send, "Resend".localize())

    open lazy var copyBt = MenuButton(.content_copy, "Copy Message".localize())

    open lazy var shareBt = MenuButton(.ios_share, "Share".localize())

    open lazy var deleteBt: MenuButton = {
        let view = MenuButton(.delete, "Delete".localize())
        view.setTitleColor(.systemRed)

        return view
    }()

    open lazy var viewProfileBt = MenuButton(.account_circle, "View Profile".localize())


    @discardableResult
    open func showIn(_ view: UIView, _ room: MXRoom?, _ roomBubbleData: RoomBubbleData,
                     callback: @escaping (_ action: Action) -> Void) -> Self
    {
        self.room = room
        self.roomBubbleData = roomBubbleData
        self.callback = callback

        backgroundColor = UIColor(white: 0, alpha: 0.5)
        translatesAutoresizingMaskIntoConstraints = false

        view.addSubview(self)
        autoPinEdgesToSuperviewEdges()

        closeTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(tappedClose))
        closeTapRecognizer?.delegate = self
        addGestureRecognizer(closeTapRecognizer!)

        self.addSubview(menu)

        menuBottomConstraint = menu.autoPinEdge(toSuperviewEdge: .bottom, withInset: 0)
        menu.autoPinEdge(toSuperviewEdge: .leading)
        menu.autoPinEdge(toSuperviewEdge: .trailing)

        if let senderId = roomBubbleData.senderId {
            name.text = roomBubbleData.getName(senderId)
        }
        else {
            name.text = "Unknown Sender".localize()
        }

        if let age = roomBubbleData.event?.age {
            timestamp.text = Formatters.format(lastActive: Double(age) / 1000)
        }
        else {
            timestamp.text = nil
        }

        content.attributedText = roomBubbleData.formattedText


        closeTapRecognizer?.isEnabled = true

        menu.layoutIfNeeded()

        menuBottomConstraint?.constant = menu.frame.height
        layoutIfNeeded()

        DispatchQueue.main.async {
            self.menuBottomConstraint?.constant = 0

            UIView.animate(withDuration: 0.3, animations: { [weak self] in
                self?.layoutIfNeeded()
            })
        }

        return self
    }


    // MARK: Actions

    @objc public func tappedClose() {
        close()
    }

    public func close(completed: (() -> Void)? = nil) {
        closeTapRecognizer?.isEnabled = false

        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            self?.menuBottomConstraint?.constant = self?.menu.frame.height ?? 0
            self?.layoutIfNeeded()
        }) { [weak self] _ in
            self?.menu.removeFromSuperview()
            self?.removeFromSuperview()

            completed?()
        }
    }

    @objc public func doAction(_ sender: MenuButton) {
        switch sender {
        case quickReactionBt:
            guard let superview = superview else {
                return
            }

            close() {
                QuickReactionPicker().showIn(superview, didPickEmoji: { [weak self] emoji in
                    self?.callback?(.reaction(emoji: emoji))
                })
            }

        case replyBt:
            if let callback = callback {
                close() {
                    callback(.reply)
                }
            }

        case forwardBt:
            if let callback = callback {
                close() {
                    callback(.forward)
                }
            }

        case resendBt:
            if let callback = callback {
                close() {
                    callback(.resend)
                }
            }

        case copyBt:
            if let text = content.attributedText,
               let rtf = try? text.data(from: NSMakeRange(0, text.length),
                                        documentAttributes: [.documentType : NSAttributedString.DocumentType.rtf]),
               let encoded = NSString(data: rtf, encoding: String.Encoding.utf8.rawValue)
            {
                UIPasteboard.general.items = [
                    [kUTTypeRTF as String: encoded],
                    [kUTTypeUTF8PlainText as String: text.string]
                ]

                close()
            }

        case shareBt:
            if let callback = callback {
                close() {
                    callback(.share)
                }
            }

        case deleteBt:
            if let callback = callback {
                close() {
                    callback(.delete)
                }
            }

        case viewProfileBt:
            if let callback = callback {
                close() {
                    callback(.viewProfile)
                }
            }

        default:
            break
        }
    }

    @objc public func quickReaction(_ sender: UIButton) {
        if let callback = callback, let emoji = sender.currentTitle {
            close() {
                callback(.reaction(emoji: emoji))
            }
        }
    }


    // MARK: UIGestureRecognizerDelegate

    open override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if gestureRecognizer == closeTapRecognizer {
            // Close only on background tap, not when tapping anything else.
            return hitTest(gestureRecognizer.location(in: self), with: nil) == self
        }

        return super.gestureRecognizerShouldBegin(gestureRecognizer)
    }
}
