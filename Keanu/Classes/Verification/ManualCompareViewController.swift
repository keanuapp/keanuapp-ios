//
//  ManualCompareViewController.swift
//  Keanu
//
//  Created by Benjamin Erhart on 31.07.20.
//

import KeanuCore

open class ManualCompareViewController: UIViewController {

    open var session: MXSession?
    open var device: MXDeviceInfo?


    @IBOutlet weak var illustrationLb: UILabel! {
        didSet {
            illustrationLb.font = .materialIcons(ofSize: 84)
            illustrationLb.text = .devices
        }
    }

    @IBOutlet weak var deviceNameLb: UILabel!

    @IBOutlet weak var deviceIdLb: UILabel!

    @IBOutlet weak var fingerprintLb: UILabel!

    @IBOutlet weak var messageLb: UILabel! {
        didSet {
            messageLb.text = "Compare this key fingerprint with the key on the other device to complete verification."
                .localize()
        }
    }

    @IBOutlet weak var matchBt: UIButton! {
        didSet {
            matchBt.setTitle("Keys Match".localize())
        }
    }

    @IBOutlet weak var interactiveBt: UIButton! {
        didSet {
            interactiveBt.setTitle("Use Interactive Mode Instead".localize())
        }
    }

    open override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.leftBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .cancel, target: self, action: #selector(cancel))
        navigationItem.title = "Verify Device".localize()

        let deviceName = device?.displayName ?? "–"

        deviceNameLb.attributedText = NSAttributedString.withBoldLabel(
            "Name: %".localize(value: deviceName),
            onlyContent: deviceName,
            size: deviceNameLb.font.pointSize)

        let deviceId = device?.deviceId ?? "–"

        deviceIdLb.attributedText = NSAttributedString.withBoldLabel(
            "Identifier: %".localize(value: deviceId),
            onlyContent: deviceId,
            size: deviceIdLb.font.pointSize)

        fingerprintLb.attributedText = NSAttributedString.fingerprint(
            device?.fingerprint, size: fingerprintLb.font.pointSize, alignment: .center)

    }


    // MARK: Actions

    @IBAction func match() {
        guard let session = session, let device = device else {
            return
        }

        VerificationManager.shared.verify(session, device, .verified) { [weak self] error in
            DispatchQueue.main.async {
                if let error = error {
                    let vc = VerificationManager.shared.verificationVc

                    self?.navigationController?.viewControllers = [vc]

                    vc.render(.error(error))
                }
                else {
                    self?.cancel()
                }
            }
        }
    }

    @IBAction func interactive() {
        guard let session = session, let device = device else {
            return
        }

        VerificationManager.shared.interactiveVerify(session, device) { error in
            DispatchQueue.main.async {
                let vc = VerificationManager.shared.verificationVc

                self.navigationController?.viewControllers = [vc]

                vc.render(.error(error))
            }
        }
    }

    @objc func cancel() {
        if let navC = navigationController {
            navC.dismiss(animated: true)
        }
        else {
            dismiss(animated: true)
        }
    }
}
