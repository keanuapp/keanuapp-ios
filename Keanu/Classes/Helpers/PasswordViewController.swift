//
//  PasswordViewController.swift
//  Keanu
//
//  Created by Benjamin Erhart on 22.06.20.
//

import UIKit
import Eureka

open class PasswordViewController: FormViewController {

    public enum Style {
        case entry
        case confirm
        case newPassword
    }

    open class func instantiate(
        style: Style = .entry, title: String? = nil, message: String? = nil,
        prefilledPassword: String? = nil, passwordLabel: String? = nil, cancelLabel: String? = nil,
        okLabel: String? = nil, completion: ((PasswordViewController) -> Void)? = nil
    ) -> UINavigationController
    {
        let vc = PasswordViewController()
        vc.style = style
        vc.title = title
        vc.message = message
        vc.passwordLabel = passwordLabel
        vc.cancelLabel = cancelLabel
        vc.okLabel = okLabel
        vc.completion = completion

        if let prefilledPassword = prefilledPassword {
            if style == .newPassword {
                vc.oldPassword = prefilledPassword
            }
            else {
                vc.password = prefilledPassword
            }
        }

        return UINavigationController(rootViewController: vc)
    }

    open var style: Style = .entry
    open var message: String?
    open var passwordLabel: String?
    open var cancelLabel: String?
    open var okLabel: String?
    open var popOnClose = false
    open var completion: ((PasswordViewController) -> Void)?


    open var oldPassword: String? {
        get {
            if form.validate().isEmpty {
                return oldPasswordRow.value
            }

            return nil
        }
        set {
            oldPasswordRow.value = newValue
        }
    }

    open var password: String? {
        get {
            if form.validate().isEmpty,
                let password = passwordRow.value,
                !password.isEmpty
            {
                return password
            }

            return nil
        }
        set {
            passwordRow.value = newValue
        }
    }

    // MARK: Private Properties

    private lazy var oldPasswordRow = PasswordRow("old_password") {
        $0.placeholder = "Old Password".localize()
        $0.add(rule: RuleRequired())
    }

    private lazy var passwordRow = PasswordRow("password") {
        $0.placeholder = passwordLabel ?? "Password".localize()
        $0.add(rule: RuleRequired())

        if style != .entry {
            $0.add(rule: RuleMinLength(minLength: 8))
        }
    }

    open override func viewDidLoad() {
        super.viewDidLoad()

        form
        +++ Section(message)

        if style == .newPassword {
            form.last!
                <<< oldPasswordRow
                .cellSetup() { cell, row in
                    cell.accessoryType = .detailButton
                }
                .onRowValidationChanged(checkRowValidity)
        }

        form.last!
            <<< passwordRow
            .cellSetup() { cell, row in
                cell.accessoryType = .detailButton
            }
            .onRowValidationChanged(checkRowValidity)

        if style == .confirm || style == .newPassword {
            form.last!
                <<< PasswordRow("confirm") {
                    $0.placeholder = "Confirm".localize()
                    $0.add(rule: RuleRequired())
                    $0.add(rule: RuleEqualsToRow(form: form, tag: "password"))
                }
                .cellSetup() { cell, row in
                    cell.accessoryType = .detailButton
                }
                .onRowValidationChanged(checkRowValidity)
        }

        form
            +++ ButtonRow() {
                $0.title = okLabel ?? "OK".localize()
                $0.disabled = true
                $0.disabled = Condition.function(["old_password", "password", "confirm"],
                                                 { !$0.validate(quietly: true).isEmpty })
            }
            .onCellSelection { [weak self] _, row in
                guard !row.isDisabled else {
                    return
                }

                if self?.form.validate().isEmpty ?? false {
                    self?.close()
                }
            }


        if let cancelLabel = cancelLabel {
            navigationItem.leftBarButtonItem = UIBarButtonItem(
                title: cancelLabel, style: .plain, target: self, action: #selector(cancel))
        }
        else {
            navigationItem.leftBarButtonItem = UIBarButtonItem(
                barButtonSystemItem: .cancel, target: self, action: #selector(cancel))
        }
    }

    open func checkRowValidity(cell: BaseCell, row: BaseRow) {
        if #available(iOS 13.0, *) {
            // Handle dark mode
            cell.backgroundColor = row.isValid ? .secondarySystemGroupedBackground : .orange
        }
        else {
            cell.backgroundColor = row.isValid ? .white : .orange
        }
    }


    // MARK: Actions

    @objc open func close() {
        if let navC = navigationController {

            if popOnClose {
                completion?(self)
                navC.popViewController(animated: true)
            }
            else {
                navC.dismiss(animated: true) {
                    self.completion?(self)
                }
            }
        }
        else {
            dismiss(animated: true) {
                self.completion?(self)
            }
        }
    }

    @objc open func cancel() {
        password = nil

        close()
    }


    // MARK: UITableViewDelegate

    /**
    Workaround to avoid capitalization of header.
    */
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if section == 0,
            let header = view as? UITableViewHeaderFooterView
        {
            header.textLabel?.text = message
        }
    }

    /**
     Toggles password visibility on tap.
     */
    func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? PasswordCell {
            cell.textField.isSecureTextEntry = !cell.textField.isSecureTextEntry
        }
    }
}
