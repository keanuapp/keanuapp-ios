//
//  MXKAttachment+Keanu.swift
//  Keanu
//
//  Created by N-Pex on 08.02.19.
//  Copyright © 2019 Guardian Project. All rights reserved.
//

import KeanuCore

extension MXKAttachment {
    
    /**
     Shortcut for checking if the attachment has been downloaded (i.e. a file exists at the cache path)
     */
    public var isDownloaded: Bool {
        guard let cacheFilePath = cacheFilePath else {
            return false
        }

        return (try? URL(fileURLWithPath: cacheFilePath).checkResourceIsReachable()) ?? false
    }
    
    public func downloadData(
        success: @escaping (_ data: Data?) -> Void,
        failure: @escaping (_ error: Error?) -> Void,
        progress: @escaping (_ percentage: Int) -> Void) -> MXMediaLoader?
    {
        let nc = NotificationCenter.default
        var observer: NSObjectProtocol?
        var successCalled = false

        let success = { (data: Data?) in
            successCalled = true

            if let observer = observer {
                nc.removeObserver(observer)
            }

            progress(100)

            success(data)
        }

        let failure = { (error: Error?) in
            // Hmm, MXMediaLoader.dealloc will call MXMediaLoader.cancel which will call MXMediaLoader.setState
            // which will post a notification which will call handler in MXKAttachment which will call the
            // failure handler here! So don't do anything if we already are in success state...
            guard !successCalled else {
                return
            }

            if let observer = observer {
                nc.removeObserver(observer)
            }

            progress(0)

            failure(error)
        }

        getData(success, failure: failure)

        let loader = MXMediaManager.existingDownloader(withIdentifier: downloadId)
        if loader != nil {
            observer = nc.addObserver(
                forName: .mxMediaLoaderStateDidChange, object: loader, queue: .main,
                using:
                    { notification in
                        guard let loader = notification.object as? MXMediaLoader else {
                            return
                        }

                        let percentage = Int(100 * loader.progressValue.floatValue)
                        progress(percentage)

                        if percentage >= 100 {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
                                guard !successCalled else {
                                    return
                                }

                                // If the success callback still wasn't called, then
                                // we now fix an issue with the MatrixSDK, where the
                                // success callback never comes when trying to load the
                                // user's own attachment, right after they sent it.
                                self?.getData(success, failure: failure)
                            }
                        }
                    })
        }

        return loader
    }
    
    public func getMediaLocalFile(
        success: @escaping (_ tempPath: String, _ wasEncrypted: Bool) -> Void,
        failure: @escaping (_ error: Error?) -> Void,
        progress: @escaping (_ percentage: Int) -> Void)
    {
        let nc = NotificationCenter.default
        var observer: NSObjectProtocol?
        var successCalled = false

        let success = { (tempPath: String, wasEncrypted: Bool) in
            successCalled = true

            if let observer = observer {
                nc.removeObserver(observer)
            }

            progress(100)

            success(tempPath, wasEncrypted)
        }

        let failure = { (error: Error?) in
            // Hmm, MXMediaLoader.dealloc will call MXMediaLoader.cancel which will call MXMediaLoader.setState
            // which will post a notification which will call handler in MXKAttachment which will call the
            // failure handler here! So don't do anything if we already are in success state...
            guard !successCalled else {
                return
            }

            if let observer = observer {
                nc.removeObserver(observer)
            }

            progress(0)

            failure(error)
        }


        if isEncrypted {
            decrypt(toTempFile: { file in
                success(file!, true)
            }, failure: failure)
        } else {
            prepare({
                success(self.cacheFilePath!, false)
            }, failure: failure)
        }

        let loader = MXMediaManager.existingDownloader(withIdentifier: downloadId)
        if loader != nil {
            observer = nc.addObserver(
                forName: .mxMediaLoaderStateDidChange, object: loader, queue: .main,
                using:
                    { notification in
                        guard let loader = notification.object as? MXMediaLoader else {
                            return
                        }

                        let percentage = Int(100 * loader.progressValue.floatValue)
                        progress(percentage)
                    })
        }
    }

}
