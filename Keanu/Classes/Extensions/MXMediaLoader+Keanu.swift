//
//  MXMediaLoader+Keanu.swift
//  Keanu
//
//  Created by Benjamin Erhart on 04.04.24.
//

import MatrixSDK

extension MXMediaLoader {

    /**
     Progress value in [0, 1] range.

     In case of upload, the properties `uploadInitialRange` and `uploadRange` are taken into account in this progress value.
     */
    var progressValue: NSNumber {
        statisticsDict?[kMXMediaLoaderProgressValueKey] as? NSNumber ?? 0
    }

    /**
     The number of bytes that have already been completed by the current job.
     */
    var completedBytesCount: NSNumber {
        statisticsDict?[kMXMediaLoaderCompletedBytesCountKey] as? NSNumber ?? 0
    }

    /**
     The total number of bytes tracked for the current job.
     */
    var totalBytesCount: NSNumber {
        statisticsDict?[kMXMediaLoaderTotalBytesCountKey] as? NSNumber ?? 0
    }

    /**
     The observed data rate in Bytes/s.
     */
    var currentDataRate: NSNumber {
        statisticsDict?[kMXMediaLoaderCurrentDataRateKey] as? NSNumber ?? 0
    }
}
