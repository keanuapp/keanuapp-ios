//
//  BaseAppDelegate.swift
//  Keanu
//
//  Created by N-Pex on 28.01.19.
//  Copyright © 2019 Guardian Project. All rights reserved.
//

import KeanuCore
import UserNotifications
import BackgroundTasks
import DTTJailbreakDetection

open class BaseAppDelegate: UIResponder, UIApplicationDelegate {
    
    // MARK: UIApplicationDelegate
    
    // Delegate commands this to be optional, but we instantiate always, because
    // we need to for flow switching in `application:didFinishLaunchingWithOptions`.
    public lazy var window: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
    
    /**
     Override this for theming support, i.e. change view controller classes etc. See [Router](x-source-tag://Router) for more info.
     */
    open var router: Router {
        BaseRouter.shared
    }

    /**
     Reads the refresh task ID from the first item in the `BGTaskSchedulerPermittedIdentifiers` key in `Info.plist`.

     You can override this, but if you need to, your configuration might probably be broken.
     */
    open var refreshTaskId: String? {
        (Bundle.main.object(forInfoDictionaryKey: "BGTaskSchedulerPermittedIdentifiers") as? [String])?.first
    }

    /**
     Override this to inject your configuration *before* calling super!
    */
    open func application(_ application: UIApplication, didFinishLaunchingWithOptions
                          launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool
    {
        KeanuCore.setUpMatrix()
        KeanuCore.loadFonts()

        checkJailbreak()

        registerBackgroundTask()

        // Initialize *before* MatrixSDK, so it registers early and
        // can catch all the necessary notifications.
        _ = FriendsManager.shared

        // Initialize *before* MatrixSDK, so it registers early and
        // can catch all the necessary notifications.
        _ = VerificationManager.shared

        // Check, if there's already a Matrix account. If not, show onboarding
        // flow with sign-in/register scenes.
        // Else show main flow.
        
        window?.makeKeyAndVisible()
        
        if MXKAccountManager.shared().accounts.isEmpty {
            UIApplication.shared.startOnboardingFlow()
        }
        else {
            UIApplication.shared.startMainFlow()
        }

        return true
    }
    
    open func applicationDidEnterBackground(_ application: UIApplication) 
    {
        FriendsManager.shared.store()

        MXKAccountManager.shared().activeAccounts.forEach { account in
            account.pauseInBackgroundTask()
        }

        scheduleRefresh()
    }
    
    open func applicationDidBecomeActive(_ application: UIApplication) 
    {
        // Make sure push is set up.
        PushManager.shared.setupPush()
        
        // Restart any tasks that were paused (or not yet started) while the
        // application was inactive. If the application was previously in the
        //background, optionally refresh the user interface.
        MXKAccountManager.shared().activeAccounts.forEach { account in
            account.resume()
        }
    }
    
    open func applicationWillTerminate(_ application: UIApplication) 
    {
        FriendsManager.shared.store()
    }
    
    open func application(_ app: UIApplication, open url: URL,
                          options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool
    {
        return UrlHandler.shared.handle(url)
    }
    
    public func application(_ application: UIApplication, 
                            continue userActivity: NSUserActivity,
                            restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool
    {
        if userActivity.activityType == NSUserActivityTypeBrowsingWeb,
           let url = userActivity.webpageURL
        {
            if !UrlHandler.shared.handle(url) {
                UIApplication.shared.open(url)
            }
        }

        return true
    }
    
    
    //MARK: Push support

    open func application(_ application: UIApplication,
                          didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)
    {
        PushManager.shared.didRegister(token: deviceToken)
    }
    
    open func application(_ application: UIApplication,
                          didFailToRegisterForRemoteNotificationsWithError error: Error)
    {
        PushManager.shared.didFailToRegister(error: error)
    }
    
    /**
     Override this to inject your configuration *before* calling super!
     */
    open func application(_ application: UIApplication, didReceiveRemoteNotification
                          userInfo: [AnyHashable : Any], fetchCompletionHandler
                          completionHandler: @escaping (UIBackgroundFetchResult) -> Void)
    {
        PushManager.shared.handleRemoteNotification(userInfo, completionHandler)
    }

    /**
     This is now only called, when a silent push notification arrives.

     To test this, use `scripts/push-test.sh` in the Example project with a simulator.

     Background tasks triggered by the device iteself are handled in `registerBackgroundTask` and `scheduleRefresh`.
     */
    open func application(_ application: UIApplication, performFetchWithCompletionHandler
                          completionHandler: @escaping (UIBackgroundFetchResult) -> Void)
    {
        print("[\(String(describing: type(of: self)))] Background fetch initiated")

        handleRefresh { success in
            completionHandler(success ? .newData : .failed)
        }
    }

    /**
     Callback for when an initial view controller is created from a storyboard.

     - parameter storyboard: The storyboard loaded from.
     - parameter viewController: The view controller that was created, or nil if creation failed.
     */
    open func storyboard(_ storyboard: UIStoryboard,
                         instantiatedInitialViewController viewController: UIViewController?)
    {
        // Base implementation does nothing.
    }
    
    /**
     Callback for when a view controller is created from a storyboard using the given identifier.

     - parameter storyboard: The storyboard loaded from.
     - parameter viewController: The view controller that was created, or nil if creation failed.
     - parameter identifier: The identifier used when creating.
     */
    open func storyboard(_ storyboard: UIStoryboard,
                         instantiatedViewController viewController: UIViewController?,
                         identifier: String)
    {
        // Base implementation does nothing.
    }

    /**
     Register the background task which loads the latest events.

     This needs to be called in `didFinishLaunchingWithOptions`. (Which the `BaseAppDelegate` implements.)

     NOTE: You will need to set one background task identifier in your `Info.plist` file using the `BGTaskSchedulerPermittedIdentifiers` key.

     To debug this: https://developer.apple.com/documentation/backgroundtasks/starting_and_terminating_tasks_during_development
     `e -l objc -- (void)[[BGTaskScheduler sharedScheduler] _simulateLaunchForTaskWithIdentifier:@"keanu.bg.refresh"]`
     */
    open func registerBackgroundTask() {
        guard let id = refreshTaskId else {
            return
        }

        BGTaskScheduler.shared.register(forTaskWithIdentifier: id, using: nil) { task in
            self.handleRefresh { success in
                task.setTaskCompleted(success: success)
            }
        }
    }

    /**
     Schedules the refresh task.

     This needs to be called in `applicationDidEnterBackground`. (Which the `BaseAppDelegate` implements.)

     NOTE: You will need to set one background task identifier in your `Info.plist` file using the `BGTaskSchedulerPermittedIdentifiers` key.

     To debug this: https://developer.apple.com/documentation/backgroundtasks/starting_and_terminating_tasks_during_development
     `e -l objc -- (void)[[BGTaskScheduler sharedScheduler] _simulateLaunchForTaskWithIdentifier:@"keanu.bg.refresh"]`
     */
    open func scheduleRefresh() {
        guard let id = refreshTaskId else {
            return
        }

        let request = BGAppRefreshTaskRequest(identifier: id)
        request.earliestBeginDate = Date(timeIntervalSinceNow: 5 * 60)

        do {
            // NOTE: This does not work in the simulator. You'll need a real device!
            try BGTaskScheduler.shared.submit(request)

            print("[\(String(describing: type(of: self)))] Successfully submitted task \(id)")
        }
        catch {
            print("[\(String(describing: type(of: self)))] Error while scheduling refresh task: \(error)")
        }
    }

    /**
     Loads the latest events.

     This can either be called from the registered background task (see `registerBackgroundTask`) or from `performFetchWithCompletionHandler`.
     (Which the `BaseAppDelegate` implements.)
     */
    open func handleRefresh(_ completion: @escaping (_ success: Bool) -> Void) {
        scheduleRefresh()

        print("[\(String(describing: type(of: self)))] Background fetch initiated")

        DomainHelper.shared.prepareSessionForActiveAccounts()

        MXKAccountManager.shared().activeAccounts.forEach { account in
            let sync = {
                account.backgroundSync(
                    25000,
                    success: {
                        print("[\(String(describing: type(of: self)))] Background sync succeeded")
                        completion(true)
                    },
                    failure: { error in
                        print("[\(String(describing: type(of: self)))] Background sync failed")
                        completion(false)
                    })
            }

            // App is still running in the background.
            if account.mxSession?.state == .paused {
                sync()
            }
            // App was started fresh. Prepare session before syncing.
            else {
                account.performOnSessionReady { _ in
                    account.pauseInBackgroundTask()

                    sync()
                }
            }
        }
    }

    /**
     Do some rudimentary checks, if the device is jailbroken and inform the user about it, if so, exactly **one** time.
     */
    open func checkJailbreak() {
        let key = "jailbreak_checked_active"
        let settings = MXKAppSettings.standard().sharedUserDefaults

        if (settings?.bool(forKey: key) ?? false)
            || !DTTJailbreakDetection.isJailbroken()
        {
            return
        }

        DispatchQueue.main.async { [weak self] in
            guard let vc = self?.window?.rootViewController else {
                return
            }

            settings?.set(true, forKey: key)

            AlertHelper.present(
                vc,
                message: "We've detected that your device is jailbroken. If you're aware, ignore this message. If you're not aware, seek an expert to help you.".localize(),
                title: "Jailbroken Device Detected".localize(),
                actions: [.init(title: "Got it!", style: .default)])
        }
    }
}
