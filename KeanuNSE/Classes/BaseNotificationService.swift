//
//  BaseNotificationService.swift
//  KeanuNSE
//
//  Created by N-Pex on 17.6.22.
//

import UserNotifications
import MatrixSDK
import KeanuCore
import AudioToolbox

/**
 To test this, unfortunately a mocked push notification via `simctl push` doesn't suffice.

 You will need to use Apple's Push Notifications Developer Console:
 https://icloud.developer.apple.com/dashboard/notifications

 The easiest way is the following:

 - Use bundle identifiers and the team ID of a registered and published app.
 - Run on the simulator, so you don't need to create development certificates and profiles for it.
 - Make sure to allow push notifications in the app and in the simulator OS settings.
 - Use Debug -> Attach to process by PID or Name...
 - Use the target name of this extension.
 - The debugger will wait for the process to spawn and stop on your set breakpoints.
 - Check the log output of `PushManager#didRegister(token)`: You will need that token
   in the console.
 - Make sure the payload contains `{"mutable-content": 1}`. Otherwise this extension won't be called.
 - If the console says, the notification was held back for power consumption concerns, run the app.
   Then the notifications should be delivered.

   80cec49164aafd765f4db58379190d85d80e68bea2b58e886a65c058a788d9f567b6c27e2a198b8726efd523e3fc587858d494989faca62815c78713daf7daf49c959b36985ceb83ebf95fec95701679
  */
open class BaseNotificationService: UNNotificationServiceExtension {
    
    /**
     Make sure to set this before using the NSE!
     */
    public static var config: KeanuConfig.Type?

    private static var backgroundSyncService: MXBackgroundSyncService!
    
    var contentHandler: ((UNNotificationContent) -> Void)?
    var content: UNMutableNotificationContent?
    
    open override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
        guard let content = request.content.mutableCopy() as? UNMutableNotificationContent else {
            return contentHandler(request.content)
        }

        let rewriteMessageText = (content.body == "MESSAGE")

        // If the body is the deafult "MESSAGE", then translate this.
        if rewriteMessageText {
            content.body = "You have a new message".localize()
        }

        guard let config = BaseNotificationService.config else {
            return contentHandler(content) // Use original content
        }

        KeanuCore.setUp(with: config)
        KeanuCore.setUpMatrix()

        MXKAccountManager.shared()
        DomainHelper.shared.prepareSessionForActiveAccounts()
        
        guard let account = MXKAccountManager.shared().activeAccounts.first,
              let session = account.mxSession,
              let eventId = request.content.userInfo["event_id"] as? String,
              let roomId = request.content.userInfo["room_id"] as? String,
              let room = MXRoom(roomId: roomId, andMatrixSession: session)
        else {
            return contentHandler(content) // Use original content
        }

        self.contentHandler = contentHandler
        self.content = content

        session.event(withEventId: eventId, inRoom: roomId, { [weak self] response in
            guard response.isSuccess,
                  let event = response.value
            else {
                self?.sendAndClean()
                return
            }

            let isMention = event.shouldBeHighlighted(inSession: session)

            if isMention, rewriteMessageText {
                content.body = "Someone mentioned you".localize()
            }

            room.state { stateResult in
                guard let state = stateResult,
                      let pushRule = session.notificationCenter?.rule(matching: event, roomState: state)
                else {
                    self?.sendAndClean()
                    return
                }

                var notify = true
                var useSound = false
                var sound: String? = nil
                var vibrate = false

                (pushRule.actions as? [MXPushRuleAction])?.forEach { action in
                    if action.actionType == MXPushRuleActionTypeSetTweak {
                        switch action.parameters["set_tweak"] as? String {
                        case "sound":
                            useSound = true
                            sound = action.parameters["value"] as? String

                        case "vibration":
                            vibrate = action.parameters["value"] as? String == "1"

                        default:
                            break
                        }
                    }
                }

                if [NotificationSettings.keanuPushRuleId, NotificationSettings.keanuPushRuleIdEncrypted].contains(pushRule.ruleId) {

                    // One of our override rules! Use values from the notification settings.
                    switch NotificationSettings.shared.notifyCondition {
                    case NotificationSettings.NotificationCondition.Always:
                        if isMention {
                            useSound = NotificationSettings.shared.soundMentions
                            sound = NotificationSettings.shared.ringtoneMentions
                            vibrate = NotificationSettings.shared.vibrateMentions
                        } 
                        else {
                            useSound = NotificationSettings.shared.soundAll
                            sound = NotificationSettings.shared.ringtoneAll
                            vibrate = NotificationSettings.shared.vibrateAll
                        }

                    case NotificationSettings.NotificationCondition.Mentions:
                        if isMention {
                            useSound = NotificationSettings.shared.soundMentions
                            sound = NotificationSettings.shared.ringtoneMentions
                            vibrate = NotificationSettings.shared.vibrateMentions
                        } 
                        else {
                            notify = false
                        }

                    default:
                        notify = false
                    }
                } 
                else {
                    sound = isMention ? NotificationSettings.shared.ringtoneMentions : NotificationSettings.shared.ringtoneAll
                }

                if !notify {
                    // Don't deliver the notification to the user.
                    self?.content = nil
                    self?.sendAndClean()

                    return
                }

                if useSound {
                    if let sound = sound, sound != "default" {
                        content.sound = UNNotificationSound(named: UNNotificationSoundName(rawValue: sound))
                    }
                    else {
                        vibrate = false // Default sound already vibrates!
                        content.sound = UNNotificationSound.default
                    }
                } 
                else {
                    content.sound = nil
                }

                if vibrate {
                    UINotificationFeedbackGenerator().notificationOccurred(.success)
                }

                self?.sendAndClean()
            }
        })
    }
    
    open override func serviceExtensionTimeWillExpire() {
        // Called just before the extension will be terminated by the system.
        // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
        sendAndClean()
    }

    private func sendAndClean() {
        contentHandler?(content ?? UNMutableNotificationContent())

        contentHandler = nil
        content = nil
    }
}
